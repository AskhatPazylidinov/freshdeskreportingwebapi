﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using BLL.Services;
using FreshDeskReportApi.Options;
using Microsoft.IdentityModel.Tokens;

namespace FreshDeskReportApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        /// <summary>
        /// Сервис аутентификации
        /// </summary>
        private readonly AuthorizationService _authenticationService;

        public AccountController(AuthorizationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        /// <summary>
        /// Получение токена
        /// </summary>
        /// <param name="username">Логин</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        [HttpPost("/token")]
        public IActionResult Token(string username, string password)
        {
            var identity = _authenticationService.GetIdentity(username, password);

            if (identity == null)
                return BadRequest(new { errorText = "Неизвестный пользователь или пароль!" });

            var dateTimeNow = DateTime.UtcNow;

            // создаем JWT-токен
            var jwt = new JwtSecurityToken(AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                notBefore: dateTimeNow,
                claims: identity.Claims,
                expires: dateTimeNow.Add(TimeSpan.FromDays(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };

            return Json(response);
        }
    }
}