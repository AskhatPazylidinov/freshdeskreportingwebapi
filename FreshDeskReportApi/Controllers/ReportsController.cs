﻿using BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace FreshDeskReportApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly GetReportsService _getReportsService;
        private readonly CreateReportService _createReportService;
        private readonly TicketService _ticketService;

        public ReportsController(GetReportsService getReportsService, CreateReportService createReportService, TicketService ticketService)
        {
            _getReportsService = getReportsService;
            _createReportService = createReportService;
            _ticketService = ticketService;
        }

        [Authorize]
        [HttpGet("GetJsonReportBreakDownByTicketsTypeReport")]
        public async Task<ActionResult> GetBreakDownByTicketsTypeReportAsync()
        {
            return new JsonResult(await _getReportsService.GetJsonReportBreakDownByTicketsTypeReportAsync("Разбивка по типам заявок"));
        }

        [Authorize]
        [HttpGet("GetJsonReportMonthly")]
        public async Task<ActionResult> GetTest()
        {
            return new JsonResult(await _getReportsService.GetJsonReportMonthly("По месяцам"));
        }

        [Authorize]
        [HttpGet("GetJsonReportModularly")]
        public async Task<ActionResult> GetTestModularly()
        {
            return new JsonResult(await _getReportsService.GetJsonReportModularly("Помодульно"));
        }

        [Authorize]
        [HttpGet("GetJsonReportClientAndModule")]
        public async Task<ActionResult> GetJsonTestClientAndModule()
        {
            return new JsonResult(await _getReportsService.GetJsonReportClientAndModule("Распределение незакрытых заявок по клиентам и модулям"));
        }

        [Authorize]
        [HttpGet("GetJsonReportInDynamic")]
        public async Task<ActionResult> GetJsonTestInDynamic()
        {
            return new JsonResult(await _getReportsService.GetJsonReportInDynamic("Статусы на сегодня в динамике"));
        }

        [Authorize]
        [HttpGet("GetJsonReportByAgents")]
        public async Task<ActionResult> GetJsonTestByAgents()
        {
            return new JsonResult(await _getReportsService.GetJsonReportByAgents("Распределение незакрытых заявок по агентам"));
        }

        [HttpGet("TestDebug")]
        public async Task<ActionResult> TestDebug()
        {
            var tickets = _ticketService.GetListOfTickets();
            await _createReportService.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync();
            return Ok();
        }
    }
}
