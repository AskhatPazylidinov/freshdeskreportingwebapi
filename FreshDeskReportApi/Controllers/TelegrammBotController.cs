﻿using System.Threading.Tasks;
using BLL.Services;
using DAL.Context;

namespace FreshDeskReportApi.Controllers
{
    public class TelegrammBotController
    {
        private readonly CreateReportService _createReportService;
        private readonly TicketService _ticketService;
        private readonly ReportsContext _reportsContext;

        public TelegrammBotController(CreateReportService createReportService, TicketService ticketService, ReportsContext reportsContext)
        {
            _createReportService = createReportService;
            _ticketService = ticketService;
            _reportsContext = reportsContext;
        }

        public void StartBot()
        {
            var bot = new TelegramBotService(_createReportService,_reportsContext,_ticketService);
            Task.Run(() => bot.StartBot());
        }
    }
}
