﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace FreshDeskReportApi.Options
{
    /// <summary>
    /// Опции токена авторизации
    /// </summary>
    public class AuthOptions
    {
        /// <summary>
        /// Издатель токена
        /// </summary>
        public const string ISSUER = "MyAuthServer";

        /// <summary>
        /// Потребитель токена
        /// </summary>
        public const string AUDIENCE = "MyAuthClient";

        /// <summary>
        /// Ключ для шифрации
        /// </summary>
        private const string KEY = "mysupersecret_secretkey!123";

        /// <summary>
        /// Время жизни токена 
        /// </summary>
        public const int LIFETIME = 360; 

        /// <summary>
        /// Получение ключа защиты
        /// </summary>
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
