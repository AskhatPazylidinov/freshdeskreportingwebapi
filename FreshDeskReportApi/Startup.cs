using DAL.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Linq;
using BLL.DistributionReports;
using BLL.Services;
using BLL.Settings;
using BLL.UpdateReports;
using DAL.Models;
using FreshDeskReportApi.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Quartz;

namespace FreshDeskReportApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionScopedJobFactory();

                var jobKey1 = new JobKey("UpdateReportsPreviousDay");
                var jobKey2 = new JobKey("DistributionReports");
                var jobKey3 = new JobKey("UpdateAllReportsToday");

                q.AddJob<UpdateReportsPreviosDay>(opts => opts.WithIdentity(jobKey1));
                q.AddJob<DistributionReports>(opts => opts.WithIdentity(jobKey2));
                q.AddJob<UpdateAllReportsToday>(opts => opts.WithIdentity(jobKey3));

                q.AddTrigger(opts => opts
                    .ForJob(jobKey1)
                    .WithIdentity("UpdateReportsPreviousDay-trigger")
                    .WithCronSchedule("0 0 9 ? * MON,TUE,WED,THU,FRI *"));

                q.AddTrigger(opts => opts
                    .ForJob(jobKey2)
                    .WithIdentity("DistributionReports-trigger")
                    .WithCronSchedule("0 0 9 ? * MON,TUE,WED,THU,FRI *"));

                q.AddTrigger(opts => opts
                    .ForJob(jobKey3)
                    .WithIdentity("UpdateAllReportsToday-trigger")
                    .WithCronSchedule("0 0 * ? * *"));
            });

            services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);

            services.AddDbContext<ReportsContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // ��������, ����� �� �������������� �������� ��� ��������� ������
                        ValidateIssuer = true,

                        // ������, �������������� ��������
                        ValidIssuer = AuthOptions.ISSUER,

                        // ����� �� �������������� ����������� ������
                        ValidateAudience = true,

                        // ��������� ����������� ������
                        ValidAudience = AuthOptions.AUDIENCE,

                        // ����� �� �������������� ����� �������������
                        ValidateLifetime = true,

                        // ��������� ����� ������������
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),

                        // ��������� ����� ������������
                        ValidateIssuerSigningKey = true
                    };
                });

            services.AddControllersWithViews();

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "FreshDeskReportApi", Version = "v1" }));

            services.AddSingleton(src => new SystemCollections(companies: GetSectionArray("Companies"),
                statuses: GetSectionDictionary("Statuses"),
                modules: GetSectionArray("Modules"),
                agents: GetSectionAgents("Agents")));

            services.AddTransient<TicketService>();
            services.AddTransient<HtmlService>();
            services.AddTransient<CreateJsonReportsService>();
            services.AddTransient<GetReportsService>();
            services.AddTransient<CreateReportService>();
            services.AddTransient<CheckChangesOfReportsService>();
            services.AddTransient<UpdateReportBySheduleService>();
            services.AddTransient<UpdateAllReportsToday>();
            services.AddTransient<UpdateReportsPreviosDay>();
            services.AddTransient<AuthorizationService>();
        }

        public void Configure(IApplicationBuilder app,
            IWebHostEnvironment env,
            CreateReportService createReportService,
            TicketService ticketService,
            ReportsContext reportsContext)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FreshDeskReportApi v1"));

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());

            //var bot = new TelegrammBotController(createReportService, ticketService, reportsContext);
            //bot.StartBot();
        }

        public List<string> GetSectionArray(string sectionName)
        {
            return Configuration.GetSection(sectionName).GetChildren().Select(x => x.Value).ToList();
        }

        public Dictionary<int, string> GetSectionDictionary(string sectionName)
        {
            var lists = Configuration.GetSection(sectionName).GetChildren().Select(x => x.GetChildren().Select(y => y.Value).ToList());

            return lists.ToDictionary(list => int.Parse(list[0]), list => list[1]);
        }
        public List<Agent> GetSectionAgents(string sectionName)
        {
            var lists = Configuration.GetSection(sectionName).GetChildren().Select(x => x.GetChildren().Select(y => y.Value).ToList());

            return lists.Select(list => new Agent
                { Email = list[0], id = double.Parse(list[1]), Name = list[2] }).ToList();
        }
    }
}