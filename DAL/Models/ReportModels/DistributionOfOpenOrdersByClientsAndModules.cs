﻿using System.Collections.Generic;

namespace DAL.Models.ReportModels
{
    public class DistributionOfOpenOrdersByClientsAndModules
    {
        public string Name { get; set; }

        public Dictionary<string, Dictionary<string, double>> Company { get; set; }
    }
}
