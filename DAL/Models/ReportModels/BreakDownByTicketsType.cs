﻿
namespace DAL.Models.ReportModels
{
    public class BreakDownByTicketsTypeReport
    {
       public string Name { get; set; }

       public int ErrorBugCount { get; set; }
       public double ErrorBugPercent { get; set; }

       public int ChangeRequestCount { get; set; }
       public double ChangeRequestPercent { get; set; }

       public int SystemQuestionCount { get; set; }
       public double SystemQuestionPercent { get; set; }

       public int NewFunctionalityRequestCount { get; set; }
       public double NewFunctionalityRequestPercent { get; set; }

       public int ParametrizationQuestionCount { get; set; }
       public double ParametrizationQuestionPercent { get; set; }

       public int TotalCount { get; set; }
       public double TotalPercent { get; set; }

    }
}
