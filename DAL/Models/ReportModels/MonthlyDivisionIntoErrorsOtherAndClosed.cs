﻿using System.Collections.Generic;

namespace DAL.Models.ReportModels
{
    public class MonthlyDivisionIntoErrorsOtherAndClosed
    {
        public string Name { get; set; }

        public Dictionary<string, Dictionary<string, double>> Month { get; set; }
    }
}
