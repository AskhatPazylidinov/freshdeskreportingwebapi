﻿using System.Collections.Generic;

namespace DAL.Models.ReportModels
{
    public class DistributionOfOpenTicketsByAgents
    {
        public string Name { get; set; }

        public Dictionary<string, List<int>> ArtemChupahin { get; set; }
        public Dictionary<string, List<int>> AidaAbykeeva { get; set; }
        public Dictionary<string, List<int>> GulzanaEsenalieva { get; set; }
        public Dictionary<string, List<int>> JoomartSharabidinov { get; set; }
        public Dictionary<string, List<int>> MeerimNurlantbekova { get; set; }
        public Dictionary<string, List<int>> IlyasJienbaev { get; set; }
    }
}
