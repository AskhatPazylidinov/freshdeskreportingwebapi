﻿using System.Collections.Generic;

namespace DAL.Models.ReportModels
{
    public class Modularly
    {
        public string Name { get; set; }

        public Dictionary<string, Dictionary<string, double>> Modules { get; set; }
    }
}
