﻿using System.Collections.Generic;

namespace DAL.Models.ReportModels
{
    public class StatusesForTodayInDynamics
    {
        public string Name { get; set; }

        public Dictionary<string, List<double>> Open { get; set; }
        public Dictionary<string, List<double>> Awaiting { get; set; }
        public Dictionary<string, List<double>> AwaitingEvaluation { get; set; }
        public Dictionary<string, List<double>> AssessmentInProgress { get; set; }
        public Dictionary<string, List<double>> AssessmentSent { get; set; }
        public Dictionary<string, List<double>> Reject { get; set; }
        public Dictionary<string, List<double>> OrderConfirmed { get; set; }
        public Dictionary<string, List<double>> InWork { get; set; }
        public Dictionary<string, List<double>> SubmittedForVerification { get; set; }
        public Dictionary<string, List<double>> FeedbackFromTheClient { get; set; }
        public Dictionary<string, List<double>> Resolved { get; set; }
        public Dictionary<string, List<double>> InTheQueueForDelivery { get; set; }
        public Dictionary<string, List<double>> Closed { get; set; }
    }
}
