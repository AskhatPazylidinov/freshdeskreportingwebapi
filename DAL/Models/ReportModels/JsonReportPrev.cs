﻿using System;

namespace DAL.Models.ReportModels
{
    public class JsonReportPrev
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public DateTime DateOfCreate { get; set; }
    }
}
