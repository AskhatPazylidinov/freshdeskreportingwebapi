﻿
namespace DAL.Models
{
    public class CustomFields
    {
        public string tf { get; set; }
        public string cf { get; set; }
        public object cf_rand224414 { get; set; }
        public object cf_rand584640 { get; set; }
        public object cf_rand168709 { get; set; }
        public object cf_rand484024 { get; set; }
    }
}
