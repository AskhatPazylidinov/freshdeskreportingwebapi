﻿namespace DAL.Models
{
    public class TableOfEmployees
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public long ChatId { get; set; }
    }
}
