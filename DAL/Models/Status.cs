﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Status
    {
        public string Name { get; set; }

        public List<double> Total { get; set; }

        public List<double> CountOfPreviousDayDifference { get; set; }

        public List<double> CountForWeek { get; set; }

        public List<double> CountFromTheBeginningOfTheMonth { get; set; }
    }
}
