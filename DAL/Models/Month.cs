﻿
namespace DAL.Models
{
    public class Month
    {
        public string NameOfMonth { get; set; }
        public int NumberOfMonth { get; set; }

        public int CountOfCreatedTickets { get; set; }

        public int CountOfCreatedTicketsWithTypeError { get; set; }

        public double PercentOfTicketsWithTypeError { get; set; }

        public int CountOfTicketsWithOtherTypes { get; set; }

        public double PercentOfTicketsWithOtherType { get; set; }

        public int CountOfClosedTicketsInThisMonth { get; set; }

        public int Difference { get; set; }

        public int CountOfCustomization { get; set; }

        public double PercentOfCountOfCustomization { get; set; }
    }
}
