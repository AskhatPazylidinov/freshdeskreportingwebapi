﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class Ticket
    {
        public List<object> cc_emails { get; set; }
        public List<object> fwd_emails { get; set; }
        public List<object> reply_cc_emails { get; set; }
        public List<object> ticket_cc_emails { get; set; }
        public bool fr_escalated { get; set; }
        public bool spam { get; set; }
        public object email_config_id { get; set; }
        public object group_id { get; set; }
        public int priority { get; set; }
        public long requester_id { get; set; }
        public long? responder_id { get; set; }
        public int source { get; set; }
        public long company_id { get; set; }
        public int status { get; set; }
        public string subject { get; set; }
        public object association_type { get; set; }
        public object support_email { get; set; }
        public object to_emails { get; set; }
        public object product_id { get; set; }
        public int id { get; set; }
        public string type { get; set; }
        public DateTime due_by { get; set; }
        public DateTime fr_due_by { get; set; }
        public bool is_escalated { get; set; }
        public CustomFields custom_fields { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public object associated_tickets_count { get; set; }
        public List<object> tags { get; set; }
    }
}
