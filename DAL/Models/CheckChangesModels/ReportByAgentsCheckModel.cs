﻿using System.Collections.Generic;

namespace DAL.Models.CheckChangesModels
{
    public class ReportByAgentsCheckModel
    {
        public string Name { get; set; }

        public Dictionary<string, int> ArtemChupahin { get; set; }
        public Dictionary<string, int> AidaAbykeeva { get; set; }
        public Dictionary<string, int> GulzanaEsenalieva { get; set; }
        public Dictionary<string, int> JoomartSharabidinov { get; set; }
        public Dictionary<string, int> MeerimNurlantbekova { get; set; }
        public Dictionary<string, int> IlyasJienbaev { get; set; }
    }
}
