﻿using System.Collections.Generic;

namespace DAL.Models.CheckChangesModels
{
    public class StatusesForTodayInDynamicsCheckModel
    {
        public string Name { get; set; }
        public Dictionary<string, double> Open { get; set; }
        public Dictionary<string, double> Awaiting { get; set; }
        public Dictionary<string, double> AwaitingEvaluation { get; set; }
        public Dictionary<string, double> AssessmentInProgress { get; set; }
        public Dictionary<string, double> AssessmentSent { get; set; }
        public Dictionary<string, double> Reject { get; set; }
        public Dictionary<string, double> OrderConfirmed { get; set; }
        public Dictionary<string, double> InWork { get; set; }
        public Dictionary<string, double> SubmittedForVerification { get; set; }
        public Dictionary<string, double> FeedbackFromTheClient { get; set; }
        public Dictionary<string, double> Resolved { get; set; }
        public Dictionary<string, double> InTheQueueForDelivery { get; set; }
        public Dictionary<string, double> Closed { get; set; }
    }
}
