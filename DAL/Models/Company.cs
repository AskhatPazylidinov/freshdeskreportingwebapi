﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Company
    {
        public double id { get; set; }

        public string name { get; set; }

        public Dictionary<string, int> Modules { get; set; } = new Dictionary<string, int>
        {
            {"Кредиты",0 },
            {"РКО",0 },
            {"АУР",0 },
            {"Бухгалтерия",0 },
            {"Депозиты/Расчетные счета",0 },
            {"Интернет банкинг",0 },
            {"Карты",0 },
            {"Касса",0 },
            {"Клиенты",0 },
            {"Комплаенс",0 },
            {"Справочные данные",0 },
            {"Сервис",0 },
            {"Отчеты",0 },
            {"Залоги",0 },
            {"ПРБО",0 },
            {"Базовые справочники",0 },
            {"Эл.кошелек",0 },
            {"ЭЦП",0 },
            {"Казначейские операции",0 },
            {"Integration Service",0 }
        };
    }
}
