﻿
namespace DAL.Models
{
    public class Module
    {
        public string Name { get; set; }

        public double Percent { get; set; }

        public int Total { get; set; }

        public int CountOfOpenedStatus { get; set; }

        public int CountOfClosedStatus { get; set; }

        public int CountOfOtherStatus { get; set; }
    }
}
