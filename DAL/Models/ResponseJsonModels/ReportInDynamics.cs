﻿
namespace DAL.Models.ResponseJsonModels
{
    public class ReportInDynamics
    {
        public string Status { get; set; }

        public int Total { get; set; }

        public double Percent { get; set; }

        public int PreviosDayGet { get; set; }

        public int PreviosDaySet { get; set; }

        public int ForWeekGet { get; set; }

        public int ForWeekSet { get; set; }

        public int ForMonthkGet { get; set; }

        public int ForForMonthSet { get; set; }
    }
}
