﻿
namespace DAL.Models.ResponseJsonModels
{
    public class ReportsByAgents
    {
        public string Name { get; set; }

        public int OpenGet { get; set; }
        public int OpenSet { get; set; }
        public int OpenStatic { get; set; }

        public int AwaitGet { get; set; }
        public int AwaitSet { get; set; }
        public int AwaitStatic { get; set; }

        public int AwaitingEvaluationGet { get; set; }
        public int AwaitingEvaluationSet { get; set; }

        public int AssessmentInProgressGet { get; set; }
        public int AssessmentInProgressSet { get; set; }


        public int AssessmentSentGet { get; set; }
        public int AssessmentSentSet { get; set; }

        public int Reject { get; set; }


        public int OrderConfirmedGet { get; set; }
        public int OrderConfirmedSet { get; set; }

        public int InWorkGet { get; set; }
        public int InWorkSet { get; set; }

        public int SubmittedForVerificationGet { get; set; }
        public int SubmittedForVerificationSet { get; set; }

        public int FeedbackFromTheClientGet { get; set; }
        public int FeedbackFromTheClientSet { get; set; }

        public int ResolvedGet { get; set; }
        public int ResolvedSet { get; set; }

        public int InTheQueueForDeliveryGet { get; set; }
        public int InTheQueueForDeliverySet { get; set; }

        public int Closed { get; set; }
    }
}
