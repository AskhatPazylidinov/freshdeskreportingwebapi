﻿
namespace DAL.Models.ResponseJsonModels
{
    public class ClientAndModules
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public int Credits { get; set; }
        public int RKO { get; set; }
        public int AUR { get; set; }
        public int AccountingDepartment { get; set; }
        public int DepositsSettlement { get; set; }
        public int IB { get; set; }
        public int Carts { get; set; }
        public int Cashbox { get; set; }
        public int Clients { get; set; }
        public int Complaens { get; set; }
        public int ReferenceData { get; set; }
        public int Service { get; set; }
        public int Reports { get; set; }
        public int Pledges { get; set; }
        public int PRBO { get; set; }
        public int BasicGuides { get; set; }
        public int EWallet { get; set; }
        public int ACP { get; set; }
        public int TreasuryOperations { get; set; }
        public int IntegrationService { get; set; }
        public int Total { get; set; }
    }
}
