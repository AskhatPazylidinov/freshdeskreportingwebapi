﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Agent
    {
        public double id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Dictionary<string, List<int>> tickets { get; set; } = new Dictionary<string, List<int>>();
    }
}
