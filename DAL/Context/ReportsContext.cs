﻿using DAL.Models;
using DAL.Models.ReportModels;
using Microsoft.EntityFrameworkCore;

namespace DAL.Context
{
    public sealed class ReportsContext : DbContext
    {
        public DbSet<JsonReportNow> Reports { get; set; }
        public DbSet<JsonReportPrev> ReportsPreviosDays { get; set; }

        public DbSet<TableOfEmployees> TableOfEmployees { get; set; }

        public ReportsContext(DbContextOptions<ReportsContext> options)
            : base(options)
        {
            Database.Migrate();
        }
    }
}
