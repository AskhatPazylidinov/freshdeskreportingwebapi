﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DAL.Context;
using DAL.Models;
using DAL.Models.CheckChangesModels;
using DAL.Models.ReportModels;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services
{
    public class CreateJsonReportsService
    {
        private readonly ReportsContext _reportsContext;
        private readonly CheckChangesOfReportsService _changesOfReportsService;

        public CreateJsonReportsService(ReportsContext reportsContext, CheckChangesOfReportsService changesOfReportsService)
        {
            _reportsContext = reportsContext;
            _changesOfReportsService = changesOfReportsService;
        }

        public async Task<ReportByAgentsCheckModel> CreateJsonReportDistributionOfOpenTicketsByAgentsAsync(List<Agent> reportByAgents)
        {
            var reportDistributionOfOpenTicketsByAgents = new DistributionOfOpenTicketsByAgents
            {
                Name = "Распределение незакрытых заявок по агентам",
                ArtemChupahin = reportByAgents.First(x => x.Name == "Артем Чупахин").tickets,
                AidaAbykeeva = reportByAgents.First(x => x.Name == "Аида Абыкеева").tickets,
                GulzanaEsenalieva = reportByAgents.First(x => x.Name == "Гульзана Эсеналиева").tickets,
                JoomartSharabidinov = reportByAgents.First(x => x.Name == "Жоомарт Шарабидинов").tickets,
                MeerimNurlantbekova = reportByAgents.First(x => x.Name == "Мээрим Нурлантбекова").tickets,
                IlyasJienbaev = reportByAgents.First(x => x.Name == "Ильяс Джиенбаев").tickets
            };

            var checkedReport = _changesOfReportsService.CheckChangesOfReportByAgents(reportDistributionOfOpenTicketsByAgents);

            var report = new JsonReportNow
            {
                Name = checkedReport.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(checkedReport)
            };

            var previosReport = await _reportsContext.Reports.FirstOrDefaultAsync(x => x.Name == report.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(report);
            await _reportsContext.SaveChangesAsync();

            return checkedReport;
        }

        public async Task<StatusesForTodayInDynamicsCheckModel> CreateJsonReportStatusesForTodayInDynamicsAsync(List<Status> listOfStatuses)
        {
            var totalOfAllStatuses = listOfStatuses.Aggregate<Status, double>(0, (current, status) => current + status.Total.Count);

            var a = listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.Total).FirstOrDefault().Count();
            var b = a / totalOfAllStatuses;

            var reportStatusesForTodayInDynamics = new StatusesForTodayInDynamics
            {
                Name = "Статусы на сегодня в динамике",
                Open = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Awaiting = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double> {listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AwaitingEvaluation = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double> {listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.Total.Count()).FirstOrDefault() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AssessmentInProgress = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AssessmentSent = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Reject = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                OrderConfirmed = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                InWork = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                SubmittedForVerification = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                FeedbackFromTheClient = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Resolved = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                InTheQueueForDelivery = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Closed = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.Total).FirstOrDefault().Count() / totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                }
            };

            var checkedReport = _changesOfReportsService.CheckChangesOfReportStatusesForTodayInDynamics(reportStatusesForTodayInDynamics);

            var report = new JsonReportNow
            {
                Name = checkedReport.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(checkedReport)
            };

            var previosReport = await _reportsContext.Reports.FirstOrDefaultAsync(x => x.Name == report.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(report);
            await _reportsContext.SaveChangesAsync();

            return checkedReport;
        }

        public async Task CreateJsonReportBreakdownByTicketsTypeAsync(Dictionary<string, int> globalReport, int countOfRecordsInGlobalReport)
        {
            var reportBreakdownByTicketsType = new BreakDownByTicketsTypeReport
            {
                Name = "Разбивка по типам заявок",
                ErrorBugCount = globalReport.Where(x => x.Key == "Ошибка/bug").Select(y => y.Value).First(),
                ErrorBugPercent = Math.Round(
                    (double) globalReport
                        .Where(x => x.Key == "Ошибка/bug")
                        .Select(y => y.Value)
                        .First() / countOfRecordsInGlobalReport * 100d, 1),

                ChangeRequestCount =
                    globalReport.Where(x => x.Key == "Запрос на изменение").Select(y => y.Value).First(),
                ChangeRequestPercent = Math.Round(
                    (double) globalReport
                        .Where(x => x.Key == "Запрос на изменение")
                        .Select(y => y.Value)
                        .First() / countOfRecordsInGlobalReport * 100d, 1),

                SystemQuestionCount =
                    globalReport.Where(x => x.Key == "Вопрос по системе").Select(y => y.Value).First(),
                SystemQuestionPercent = Math.Round(
                    (double) globalReport
                        .Where(x => x.Key == "Вопрос по системе")
                        .Select(y => y.Value)
                        .First() / countOfRecordsInGlobalReport * 100d, 1),

                NewFunctionalityRequestCount = globalReport.Where(x => x.Key == "Запрос нового функционала")
                    .Select(y => y.Value).First(),
                NewFunctionalityRequestPercent = Math.Round(
                    (double) globalReport
                        .Where(x => x.Key == "Запрос нового функционала")
                        .Select(y => y.Value)
                        .First() / countOfRecordsInGlobalReport * 100d, 1),

                ParametrizationQuestionCount = globalReport.Where(x => x.Key == "Вопрос по параметризации")
                    .Select(y => y.Value).First(),
                ParametrizationQuestionPercent = Math.Round(
                    (double) globalReport
                        .Where(x => x.Key == "Вопрос по параметризации")
                        .Select(y => y.Value)
                        .First() / countOfRecordsInGlobalReport * 100d, 1),

                TotalCount = countOfRecordsInGlobalReport,
                TotalPercent = 100
            };

            var report = new JsonReportNow
            {
                Name = reportBreakdownByTicketsType.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportBreakdownByTicketsType)
            };

            var previosReport = await _reportsContext.Reports.FirstOrDefaultAsync(x => x.Name == reportBreakdownByTicketsType.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(report);
            await _reportsContext.SaveChangesAsync();
        }

        public async Task CreateJsonReportMonthlyDivisionIntoErrorsOtherAndClosedAsync(List<Month> reportsByMonth)
        {
            var culture = new CultureInfo("ru-RU");

            var dictionaryOfMonth = new Dictionary<string, Dictionary<string, double>>();

            foreach (var report in reportsByMonth)
            {
                var name = new DateTime(DateTime.Now.Year, reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.NumberOfMonth).FirstOrDefault(), 01).ToString("MMM", culture);
                var values = new Dictionary<string, double>
                {
                    {"Всего", reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.CountOfCreatedTickets).FirstOrDefault()},
                    {"Ошибки", reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.CountOfCreatedTicketsWithTypeError).FirstOrDefault()},
                    {"Другое", reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.CountOfTicketsWithOtherTypes).FirstOrDefault()},
                    {"Закрыто в этом месяце", reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.CountOfClosedTicketsInThisMonth).FirstOrDefault()},
                    {"Разница", reportsByMonth.Where(x => x.NumberOfMonth == report.NumberOfMonth).Select(y => y.Difference).FirstOrDefault()}
                };
                dictionaryOfMonth.Add(name, values);
            }

            var reportMonthlyDivisionIntoErrorsOtherAndClosed = new MonthlyDivisionIntoErrorsOtherAndClosed
            {
                Name = "По месяцам",
                Month = dictionaryOfMonth
            };

            var reportJson = new JsonReportNow
            {
                Name = reportMonthlyDivisionIntoErrorsOtherAndClosed.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportMonthlyDivisionIntoErrorsOtherAndClosed)
            };

            var previosReport = await _reportsContext.Reports.FirstOrDefaultAsync(x => x.Name == reportMonthlyDivisionIntoErrorsOtherAndClosed.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(reportJson);
            await _reportsContext.SaveChangesAsync();
        }

        public async Task CreateJsonReportDistributionOfOpenOrdersByClientsAndModulesAsync(IEnumerable<Company> listOfCompany, List<string> companies)
        {
            var dictionaryOfCompanies = new Dictionary<string, Dictionary<string, double>>();
            var companyNum = 1;
            foreach (var company in listOfCompany)
            {
                if (!companies.Contains(company.name)) continue;

                var name = company.name;
                var values = new Dictionary<string, double>
                {
                                 {"№п/п", companyNum++},
                                 {"Кредиты", company.Modules.Where(x => x.Key == "Кредиты").Select(y => y.Value).FirstOrDefault()},
                                 {"РКО", company.Modules.Where(x => x.Key == "РКО").Select(y => y.Value).FirstOrDefault()},
                                 {"АУР", company.Modules.Where(x => x.Key == "АУР").Select(y => y.Value).FirstOrDefault()},
                                 {"Бухгалтерия", company.Modules.Where(x => x.Key == "Бухгалтерия").Select(y => y.Value).FirstOrDefault()},
                                 {"Депозиты,расчетные", company.Modules.Where(x => x.Key == "Депозиты,расчетные").Select(y => y.Value).FirstOrDefault()},
                                 {"ИБ", company.Modules.Where(x => x.Key == "ИБ").Select(y => y.Value).FirstOrDefault()},
                                 {"Карты", company.Modules.Where(x => x.Key == "Карты").Select(y => y.Value).FirstOrDefault()},
                                 {"Касса", company.Modules.Where(x => x.Key == "Касса").Select(y => y.Value).FirstOrDefault()},
                                 {"Клиенты", company.Modules.Where(x => x.Key == "Клиенты").Select(y => y.Value).FirstOrDefault()},
                                 {"Комплаенс", company.Modules.Where(x => x.Key == "Комплаенс").Select(y => y.Value).FirstOrDefault()},
                                 {"Справочные данные", company.Modules.Where(x => x.Key == "Справочные данные").Select(y => y.Value).FirstOrDefault()},
                                 {"Сервис", company.Modules.Where(x => x.Key == "Сервис").Select(y => y.Value).FirstOrDefault()},
                                 {"Отчеты", company.Modules.Where(x => x.Key == "Отчеты").Select(y => y.Value).FirstOrDefault()},
                                 {"Залоги", company.Modules.Where(x => x.Key == "Залоги").Select(y => y.Value).FirstOrDefault()},
                                 {"ПРБО", company.Modules.Where(x => x.Key == "ПРБО").Select(y => y.Value).FirstOrDefault()},
                                 {"Базовые справочники", company.Modules.Where(x => x.Key == "Базовые справочники").Select(y => y.Value).FirstOrDefault()},
                                 {"Эл.кошелек", company.Modules.Where(x => x.Key == "Эл.кошелек").Select(y => y.Value).FirstOrDefault()},
                                 {"ЭЦП", company.Modules.Where(x => x.Key == "ЭЦП").Select(y => y.Value).FirstOrDefault()},
                                 {"Казначейские операции", company.Modules.Where(x => x.Key == "Казначейские операции").Select(y => y.Value).FirstOrDefault()},
                                 {"Integration Service", company.Modules.Where(x => x.Key == "Integration Service").Select(y => y.Value).FirstOrDefault()},
                                 {"Итого", company.Modules.Where(x => x.Key == "Итого").Select(y => y.Value).FirstOrDefault()}
                };

                dictionaryOfCompanies.Add(name, values);
            }

            var reportDistributionOfOpenOrdersByClientsAndModules = new DistributionOfOpenOrdersByClientsAndModules
            {
                Name = "Распределение незакрытых заявок по клиентам и модулям",
                Company = dictionaryOfCompanies
            };

            var reportJson = new JsonReportNow
            {
                Name = reportDistributionOfOpenOrdersByClientsAndModules.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportDistributionOfOpenOrdersByClientsAndModules)
            };

            var previosReport = _reportsContext.Reports.FirstOrDefault(x => x.Name == reportDistributionOfOpenOrdersByClientsAndModules.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(reportJson);
            await _reportsContext.SaveChangesAsync();
        }

        public async Task CreateJsonReportModularlyAsync(List<Module> listOfModules)
        {
            var dictionaryOfModule = new Dictionary<string, Dictionary<string, double>>();

            var totalOfModulesValues = listOfModules.Sum(x => x.Total);

            foreach (var module in listOfModules)
            {
                    var name = module.Name;
                    var values = new Dictionary<string, double>
                    {
                        {"В%", (double) module.Total / totalOfModulesValues * 100d},
                        {"Всего", module.Total},
                        {"Открыто", module.CountOfOpenedStatus},
                        {"Закрыто", module.CountOfClosedStatus},
                        {"Другое", module.CountOfOtherStatus}
                    };

                    dictionaryOfModule.Add(name, values);
            }

            var reportModularly = new Modularly
            {
                Name = "Помодульно",
                Modules = dictionaryOfModule
            };

            var reportJson = new JsonReportNow
            {
                Name = reportModularly.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportModularly)
            };

            var previosReport = await _reportsContext.Reports.FirstOrDefaultAsync(x => x.Name == reportModularly.Name);
            if (previosReport != null)
                _reportsContext.Reports.Remove(previosReport);

            await _reportsContext.Reports.AddAsync(reportJson);
            await _reportsContext.SaveChangesAsync();
        }
    }
}
