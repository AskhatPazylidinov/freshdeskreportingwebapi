﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;
using DAL.Models.CheckChangesModels;

namespace BLL.Services
{
    public class HtmlService
    {
        private static readonly DateTime DateNow = DateTime.Now;
        private readonly DateTime _startDate = new DateTime(DateNow.Year, 01, 01);
        private static readonly int EndMonth = Math.Max(1, DateNow.Month - 1);
        private readonly DateTime _endDate = new DateTime(DateNow.Year, EndMonth, DateTime.DaysInMonth(DateNow.Year, EndMonth)).AddDays(1);

        private void DeleteHtmlFile(string nameOfReport)
        {
            var path = $"E:\\FreshdeskReportsAPI\\Reports\\{nameOfReport}.html";
            var fileInf = new FileInfo(path);
            if (fileInf.Exists)
             fileInf.Delete();
        }

        public async Task CreateHtmlFileBreakdownByTicketsTypeAsync(Dictionary<string, int> report, int total)
        {
            DeleteHtmlFile("Разбивка_по_типам_заявок");
            string bloks = null;
            foreach ((var key, var value) in report)
            {
                var percent = (double) value / total * 100d;
                percent = Math.Round(percent, 1);
                bloks += "   <tr>\n" +
                         $"<td>{key}</td>\n" +
                         $"<td>{value}</td>\n" +
                         $"<td>{percent}%</td>\n" +
                         $"   </tr>\n";
            }


            var streamwriter = new StreamWriter(@"E:\FreshdeskReportsAPI\Reports\Разбивка_по_типам_заявок.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Отчет по периоду с {_startDate:yyyy MMMM dd} по {_endDate:yyyy MMMM dd}</p>\n" +
                                   $" <table class=\"table\">\n" +
                                   $" <thead>\n" +
                                   $"<tr>\n" +
                                   $"<th>Тип заявки</th>\n" +
                                   $"<th>Количество</th>\n" +
                                   $"<th>Процент от общего кол-ва</th>\n" +
                                   $"</tr>\n" +
                                   $"\n" +
                                   $" </thead >\n" +
                                   $" <tbody>\n" +
                                   $"{bloks}\n" +
                                   "   <tr>\n" +
                                   "<td>Всего</td>\n" +
                                   $"<td>{total}</td>\n" +
                                   "<td>100%</td>\n" +
                                   "   </tr>\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileDistributionOfOpenTicketsByAgentsAsync(ReportByAgentsCheckModel report)
        {
            var reportDateTime = DateTime.Now.ToString("yyyy-MM-dd");
            DeleteHtmlFile($"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")})");

            var streamwriter = new StreamWriter($"E:\\FreshdeskReportsAPI\\Reports\\Распределние_незакрытых_заявок_по_агентам_({DateTime.Now.ToString("yyyy - MM - dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                                   "<style type=\"text/css\">\n" +
                                  ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "  margin-left: 200px;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 45%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Распределение незакрытых заявок по агентам ({reportDateTime})<p>\n" +
                                   " <table class=\"table\">\n" +
                                   "<thead> \n" +
                                    "<tr> \n" +
                                    "<th>№п/п</th> \n" +
                                    "<th>Агент</th> \n" +
                                    "<th colspan=\"3\">Открыто</th> \n" +
                                    "<th colspan=\"3\">В ожидании</th> \n" +
                                    "<th colspan=\"2\" id=\"priority\">Ожидает оценки</th> \n" +
                                    "<th colspan=\"2\" id=\"priority\">Идет оценка</th> \n" +
                                    "<th colspan=\"2\">Оценка отправлена</th> \n" +
                                    "<th>Отказ</th> \n" +
                                    "<th colspan=\"2\">Заказ подтвержден</th> \n" +
                                    "<th colspan=\"2\">В работе</th> \n" +
                                    "<th colspan=\"2\">Передано на проверку</th> \n" +
                                    "<th colspan=\"2\">Feedback от клиента</th> \n" +
                                    "<th colspan=\"2\">В очереди на поставку</th> \n" +
                                    "<th>Закрыто</th> \n" +
                                     "</thead > \n" +
                                     "<tbody> \n" +
                                    "<tr> \n" +
                                    $"<td>-</td> \n" +
                                    $"<td>-</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Остаток</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Остаток</td> \n" +
                                    "<td id=\"priority\">Приход</td> \n" +
                                    "<td id=\"priority\">Расход</td> \n" +
                                    "<td id=\"priority\">Приход</td> \n" +
                                    "<td id=\"priority\">Расход</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>-</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>Приход</td> \n" +
                                    $"<td>Расход</td> \n" +
                                    $"<td>-</td> \n" +
                                     "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>1</td> \n" +
                                    $"<td>Артем Чупахин</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" + 
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.ArtemChupahin.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.ArtemChupahin.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.ArtemChupahin.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.ArtemChupahin.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.ArtemChupahin.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>2</td> \n" +
                                    $"<td>Аида Абыкеева</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.AidaAbykeeva.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.AidaAbykeeva.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.AidaAbykeeva.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>3</td> \n" +
                                    $"<td>Гульзана Эсеналиева</td> \n" +
                                  $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.GulzanaEsenalieva.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>4</td> \n" +
                                    $"<td>Жоомарт Шарабидинов</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.JoomartSharabidinov.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.JoomartSharabidinov.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.JoomartSharabidinov.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>5</td> \n" +
                                    $"<td>Мээрим Нурлантбекова</td> \n" +
                                   $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.MeerimNurlantbekova.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                       "<tr> \n" +
                                    $"<td>6</td> \n" +
                                    $"<td>Ильяс Джиенбаев</td> \n" +
                                     $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.IlyasJienbaev.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td id=\"priority\">{report.IlyasJienbaev.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                    $"<td>{report.IlyasJienbaev.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()}</td> \n" +
                                       "</tr> \n" +
                                     "</tbody> \n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileStatusesForTodayInDynamicsAsync(StatusesForTodayInDynamicsCheckModel report)
        {
            DeleteHtmlFile($"Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")})");

            var total = report.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()
                        + report.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault() 
                        + report.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault();
            
            var streamwriter = new StreamWriter($"E:\\FreshdeskReportsAPI\\Reports\\Статусы_на_сегодня_в_динамике_({DateTime.Now.ToString("yyyy-MM-dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                  "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin-left: 15%;\n" +
                                   "margin-top: 5%;\n" +
                                   "text-align: center;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                  $"<body>\n" +
                                    $"<div class=\"centered\">\n" +
                                    $"<p style=\"margin-left: 20%\">Статусы на 9:00 утра сегодня(2021 - 07 - 02) в динамике</p>\n" +
                                     $"<table class=\"table\">\n" +
                                     $"<thead>\n" +
                                    $"<tr>\n" +
                                    $"<th>Тип заявки</th>\n" +
                                    $"<th>Количество</th>\n" +
                                    $"<th>Процент от общего кол-ва</th>\n" +
                                    $"<th colspan = \"2\" > По сравнению с предыдущим днем</th>\n" +
                                    $"<th colspan = \"2\" > За неделю</th>\n" +
                                    $"<th colspan = \"2\" > За месяц</th>\n" +
                                    $"</tr>\n" +
                                     $"</thead>\n" +
                                     $"<tbody>\n" +
                                    $"<tr>\n" +
                                    $"<td>-</td>\n" +
                                    $"<td>{total}</td>\n" +
                                    $"<td>100%</td>\n" +
                                    $"<td>Приход</td>\n" +      
                                    $"<td>Расход</td>\n" +
                                    $"<td>Приход</td>\n" +
                                    $"<td>Расход</td>\n" +
                                    $"<td>Приход</td>\n" +
                                    $"<td>Расход</td>\n" +
                                    $"</tr>\n" +
                                    $"   <tr>\n" +
                                    $"<td>Открыто</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"<td>{report.Open.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                      $" <tr>\n" +
                                    $"<td>В ожидании</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Awaiting.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr id=\"priority\">\n" +
                                    $"<td> Ожидает оценки</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AwaitingEvaluation.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr id = \"priority\" >\n" +
                                    $"<td> Идет оценка</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentInProgress.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                      $" <tr>\n" +
                                    $"<td>Оценка отправлена</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.AssessmentSent.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                    $"   </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>Отказ</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Reject.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>Заказ подтвержден</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.OrderConfirmed.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>В работе</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InWork.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>Передано на проверку</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.SubmittedForVerification.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>Feedback от клиента</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.FeedbackFromTheClient.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                    $"   <tr>\n" +
                                    $"<td>В очереди на поставку</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.InTheQueueForDelivery.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"  <tr>\n" +
                                    $"<td>Закрыто</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                   $"<td>{report.Closed.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()}</td>\n" +
                                     $"  </tr>\n" +
                                     $"</tbody>\n" +
                                     $"</table>\n" +
                                     $"</div>\n" +
                                    $"</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileMonthlyDivisionIntoErrorsOtherAndClosedAsync(List<Month> months)
        {
            DeleteHtmlFile($"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now:yyyy - MM - dd})");
            var culture = new CultureInfo("ru-RU");

            string bloks = null;
            foreach (var month in months)
            {
                var monthName = new DateTime(DateTime.Now.Year, month.NumberOfMonth, 01);
                bloks += $"<tr>\n" +
                         $"<td>{monthName.ToString("MMM", culture)}</td>\n" +
                         $"<td>{month.CountOfCreatedTickets}</td>\n" +
                         $"<td>{month.CountOfCustomization} ({month.PercentOfCountOfCustomization}%)</td>\n" +
                         $"<td>{month.CountOfCreatedTicketsWithTypeError} ({month.PercentOfTicketsWithTypeError}%)</td>\n" +
                         $"<td>{month.CountOfTicketsWithOtherTypes}       ({month.PercentOfTicketsWithOtherType}%)</td>\n" +
                         $"<td>{month.CountOfClosedTicketsInThisMonth}</td>\n" +
                         $"<td>{month.Difference}</td>\n" +
                         $"</tr>";
            }

            var streamwriter = new StreamWriter($"E:\\FreshdeskReportsAPI\\Reports\\Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now.ToString("yyyy - MM - dd")}).html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  margin: auto;\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "text-align: center;\n" +
                                   "padding-left: 25px;\n" +
                                   "padding-right: 25px;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода ({DateTime.Now.ToString("yyyy - MM - dd")})</p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>Создано</th>\n" +
                                   $"<th>Всего</th>\n" +
                                   $"<th>Кастомизация</th>\n" +
                                   "<th>Ошибки</th>\n" +
                                   "<th>Другое</th>\n" +
                                   "<th>Закрыто в этом месяце</th>\n" +
                                   "<th>Разница</th>\n" +
                                   "</tr>\n" +
                                   " </thead>\n" +
                                   " <tbody>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileDistributionOfOpenOrdersByClientsAndModulesAsync(List<Company> listOfCompanies, List<string> companies)
        {
            DeleteHtmlFile($"Распределение_незакрытых_заявок_по_клиентам_и_модулям");

            var currentNum = 1;
            string bloks = null;
            foreach (var company in listOfCompanies)
            {
                if (companies.Contains(company.name))
                 bloks += $"<tr>\n" +
                  $"<td>{currentNum++}</td>\n" +
                  $"<td>{company.name}</td>\n" +
                  $"<td>{company.Modules["Кредиты"]}</td>\n" +
                  $"<td>{company.Modules["РКО"]}</td>\n" +
                  $"<td>{company.Modules["АУР"]}</td>\n" +
                  $"<td>{company.Modules["Бухгалтерия"]}</td>\n" +
                  $"<td>{company.Modules["Депозиты/Расчетные счета"]}</td>\n" +
                  $"<td>{company.Modules["Интернет банкинг"]}</td>\n" +
                  $"<td>{company.Modules["Карты"]}</td>\n" +
                  $"<td>{company.Modules["Касса"]}</td>\n" +
                  $"<td>{company.Modules["Клиенты"]}</td>\n" +
                  $"<td>{company.Modules["Комплаенс"]}</td>\n" +
                  $"<td>{company.Modules["Справочные данные"]}</td>\n" +
                  $"<td>{company.Modules["Сервис"]}</td>\n" +
                  $"<td>{company.Modules["Отчеты"]}</td>\n" +
                  $"<td>{company.Modules["Залоги"]}</td>\n" +
                  $"<td>{company.Modules["ПРБО"]}</td>\n" +
                  $"<td>{company.Modules["Базовые справочники"]}</td>\n" +
                  $"<td>{company.Modules["Эл.кошелек"]}</td>\n" +
                  $"<td>{company.Modules["ЭЦП"]}</td>\n" +
                  $"<td>{company.Modules["Казначейские операции"]}</td>\n" +
                  $"<td>{company.Modules["Integration Service"]}</td>\n" +
                  $"<td>{company.Modules.Values.Sum()}</td>\n" +
                  $"</tr>\n";
            }

            var streamwriter = new StreamWriter($"E:\\FreshdeskReportsAPI\\Reports\\Распределение_незакрытых_заявок_по_клиентам_и_модулям.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  margin: auto;\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "text-align: center;\n" +
                                   "padding-left: 25px;\n" +
                                   "padding-right: 25px;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Распределение незакрытых заявок по клиентам и модулям</p>\n" +
                                   " <table class=\"table\">\n" +
                                   " <thead>\n" +
                                   "<tr>\n" +
                                   "<th>№п/п</th>\n" +
                                   $"<th>Компании</th>\n" +
                                   "<th>Кредиты</th>\n" +
                                   "<th>РКО</th>\n" +
                                   "<th>АУР</th>\n" +
                                   "<th>Бухгалтерия</th>\n" +
                                   "<th>Депозиты,расчетные</th>\n" +
                                   "<th>ИБ</th>\n" +
                                   "<th>Карты</th>\n" +
                                   "<th>Касса</th>\n" +
                                   "<th>Клиенты</th>\n" +
                                   "<th>Комплаенс</th>\n" +
                                   "<th>Справочные данные</th>\n" +
                                   "<th>Сервис</th>\n" +
                                   "<th>Отчеты</th>\n" +
                                   "<th>Залоги</th>\n" +
                                   "<th>ПРБО</th>\n" +
                                   "<th>Базовые справочники</th>\n" +
                                   "<th>Э.кошелек</th>\n" +
                                   "<th>ЭЦП</th>\n" +
                                   "<th>Казначейские операции</th>\n" +
                                   "<th>Integration Service</th>\n" +
                                   "<th>Итого</th>\n" +
                                   "</tr>\n" +
                                   " </thead>\n" +
                                   " <tbody>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n" +
                                   "");
            streamwriter.Close();
        }

        public async Task CreateHtmlFileModularlyAsync(List<Module> modules)
        {
            DeleteHtmlFile("Помодульно");
            string bloks = null;
            foreach (var module in modules)
            {
                bloks += "   <tr>\n" +
                         $"<td>{module.Percent}%</td>\n" +
                         $"<td>{module.Name}</td>\n" +
                         $"<td>{module.Total}</td>\n" +
                         $"<td>{module.CountOfOpenedStatus}</td>\n" +
                         $"<td>{module.CountOfClosedStatus}</td>\n" +
                         $"<td>{module.CountOfOtherStatus}</td>\n" +
                         $"   </tr>\n";
            }

            var streamwriter = new StreamWriter(@"E:\FreshdeskReportsAPI\Reports\Помодульно.html");
            await streamwriter.WriteLineAsync("<html>\n" +
                                   "<head>\n" +
                                   "  <title>Report</title>\n" +
                                   "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +

                                   "<style type=\"text/css\">\n" +
                                   ".table{\n" +
                                   "  border: 1px solid #eee;\n" +
                                   "  table-layout: fixed;\n" +
                                   "}\n" +
                                   ".table th {\n" +
                                   "  font-weight: bold;\n" +
                                   "  padding: 5px;\n" +
                                   "  background: #efefef;\n" +
                                   "  border: 1px solid #dddddd;\n" +
                                   "}\n" +
                                   ".table td{\n" +
                                   "  padding: 5px 10px;\n" +
                                   "  text-align: left;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(odd){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".table tbody tr:nth-child(even){\n" +
                                   "  background: #fff;\n" +
                                   "}\n" +
                                   ".centered {\n" +
                                   "margin: 0;\n" +
                                   "top: 50%;\n" +
                                   "left: 50%;\n" +
                                   "transform: translate(-50%,-50%);\n" +
                                   "  background: #f0f0f0;\n" +
                                   "  text-align: center;\n" +
                                   "  position: absolute;\n" +
                                   "}\n" +
                                   "#priority{\n" +
                                   "  background-color: #ee9393;\n" +
                                   "}\n" +
                                   "</style>\n" +
                                   "</head>\n" +
                                   "<body>\n" +
                                   "<div class=\"centered\">\n" +
                                   $"<p>Помодульно</p>\n" +
                                   $" <table class=\"table\">\n" +
                                   $" <thead>\n" +
                                   $"<tr>\n" +
                                   $"<th>В%</th>\n" +
                                   $"<th>Модули</th>\n" +
                                   $"<th>Всего</th>\n" +
                                   $"<th>Открыто</th>\n" +
                                   $"<th>Закрыто</th>\n" +
                                   $"<th>Другое</th>\n" +
                                   $"</tr>\n" +
                                   $"\n" +
                                   $" </thead >\n" +
                                   $" <tbody>\n" +
                                   "   <tr>\n" +
                                   "<td>100%</td>\n" +
                                   $"<td>-</td>\n" +
                                   $"<td>{modules.Sum(x => x.Total)}</td>\n" +
                                   $"<td>{modules.Sum(x => x.CountOfOpenedStatus)}</td>\n" +
                                   $"<td>{modules.Sum(x => x.CountOfClosedStatus)}</td>\n" +
                                   $"<td>{modules.Sum(x => x.CountOfOtherStatus)}</td>\n" +
                                   "   </tr>\n" +
                                   $"{bloks}\n" +
                                   " </tbody>\n" +
                                   "  </table>\n" +
                                   "  </div>\n" +
                                   "</body>\n" +
                                   "</html>\n");
            streamwriter.Close();
        }
    }
}
