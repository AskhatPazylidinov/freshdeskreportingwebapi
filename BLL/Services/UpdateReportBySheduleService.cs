﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DAL.Context;
using DAL.Models.ReportModels;

namespace BLL.Services
{
    public class UpdateReportBySheduleService
    {
        private readonly CreateReportService _createReportService;
        private readonly TicketService _ticketService;
        private readonly CreateJsonReportsService _createJsonReportsService;
        private readonly ReportsContext _reportsContext;
        private readonly HtmlService _htmlService;

        public UpdateReportBySheduleService(HtmlService htmlService, CreateReportService createReportService, TicketService ticketService, CreateJsonReportsService createJsonReportsService, ReportsContext reportsContext)
        {
            _createReportService = createReportService;
            _ticketService = ticketService;
            _createJsonReportsService = createJsonReportsService;
            _reportsContext = reportsContext;
            _htmlService = htmlService;
        }

        public async Task UpdateAllReports()
        {
            var listOfTickets = _ticketService.GetListOfTickets();
            await _createReportService.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync();
            await _createReportService.CreateReportBreakdownByTicketsTypeAsync(listOfTickets);
            await _createReportService.CreateReportModularlyAsync(listOfTickets);
            await _createReportService.MonthlyDivisionIntoErrorsOtherAndClosedAsync(listOfTickets);

            var reportByAgent = _createReportService.CreateReportDistributionOfOpenTicketsByAgents(listOfTickets);
            var reportByAgentsJson = await _createJsonReportsService.CreateJsonReportDistributionOfOpenTicketsByAgentsAsync(reportByAgent);
            await _htmlService.CreateHtmlFileDistributionOfOpenTicketsByAgentsAsync(reportByAgentsJson);

            var report = _createReportService.CreateReportStatusesForTodayInDynamics(listOfTickets);
            var reportJson = await _createJsonReportsService.CreateJsonReportStatusesForTodayInDynamicsAsync(report);
            await _htmlService.CreateHtmlFileStatusesForTodayInDynamicsAsync(reportJson);
        }

        public async Task UpdateReportsPreviosDay()
        {
            var listOfTickets = _ticketService.GetListOfTickets();

            var listOfStatuses = _createReportService.CreateReportStatusesForTodayInDynamics(listOfTickets);
            var reportByAgents = _createReportService.CreateReportDistributionOfOpenTicketsByAgents(listOfTickets);


            var totalOfAllStatuses = listOfStatuses.Sum(status => status.Total.Count);
            
            var reportStatusesForTodayInDynamics = new StatusesForTodayInDynamics
            {
                Name = "Статусы на сегодня в динамике",
                Open = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Открыто").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Awaiting = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В ожидании").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AwaitingEvaluation = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Ожидает оценки").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AssessmentInProgress = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Идет оценка").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                AssessmentSent = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Оценка отправлена").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Reject = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Отказ").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                OrderConfirmed = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Заказ подтвержден").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                InWork = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В работе").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                SubmittedForVerification = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Передано на проверку").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                FeedbackFromTheClient = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Feedback от клиента").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Resolved = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Решено").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                InTheQueueForDelivery = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "В очереди на поставку").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                },
                Closed = new Dictionary<string, List<double>>
                {
                    {"Количество", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.Total).FirstOrDefault()},
                    {"Процент от общего кол-ва", new List<double>
                        {listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.Total.Count).FirstOrDefault() / (double) totalOfAllStatuses * 100}},
                    {"По сравнению с предыдущим днем", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountOfPreviousDayDifference).FirstOrDefault()},
                    {"За неделю", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountForWeek).FirstOrDefault()},
                    {"За месяц", listOfStatuses.Where(x => x.Name == "Закрыто").Select(y => y.CountFromTheBeginningOfTheMonth).FirstOrDefault()}
                }
            };

            var reportDynamic = new JsonReportPrev
            {
                Name = reportStatusesForTodayInDynamics.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportStatusesForTodayInDynamics)
            };

            await _reportsContext.ReportsPreviosDays.AddAsync(reportDynamic);

            var reportDistributionOfOpenTicketsByAgents = new DistributionOfOpenTicketsByAgents
            {
                Name = "Распределение незакрытых заявок по агентам",
                ArtemChupahin = reportByAgents.First(x => x.Name == "Артем Чупахин").tickets,
                AidaAbykeeva = reportByAgents.First(x => x.Name == "Аида Абыкеева").tickets,
                GulzanaEsenalieva = reportByAgents.First(x => x.Name == "Гульзана Эсеналиева").tickets,
                JoomartSharabidinov = reportByAgents.First(x => x.Name == "Жоомарт Шарабидинов").tickets,
                MeerimNurlantbekova = reportByAgents.First(x => x.Name == "Мээрим Нурлантбекова").tickets,
                IlyasJienbaev = reportByAgents.First(x => x.Name == "Ильяс Джиенбаев").tickets
            };
            
            var reportAgents = new JsonReportPrev
            {
                Name = reportDistributionOfOpenTicketsByAgents.Name,
                DateOfCreate = DateTime.Now.Date,
                Value = JsonSerializer.Serialize(reportDistributionOfOpenTicketsByAgents)
            };

            await _reportsContext.ReportsPreviosDays.AddAsync(reportAgents);
            await _reportsContext.SaveChangesAsync();
        }
    }
}
