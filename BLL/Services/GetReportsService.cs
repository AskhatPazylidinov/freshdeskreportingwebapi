﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using BLL.Settings;
using DAL.Context;
using DAL.Models;
using DAL.Models.CheckChangesModels;
using DAL.Models.ReportModels;
using DAL.Models.ResponseJsonModels;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services
{
    public class GetReportsService
    {
        private readonly ReportsContext _reportsContext;
        private readonly List<string> _agents;
        private readonly List<string> _statuses;

        public GetReportsService(ReportsContext reportsContext, SystemCollections collections)
        {
            _reportsContext = reportsContext;
            _agents = collections.Agents.Select(y => y.Name).ToList();
            _statuses = collections.Statuses.Select(y => y.Value).ToList();
        }

        public async Task<BreakDownByTicketsTypeReport> GetJsonReportBreakDownByTicketsTypeReportAsync(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<BreakDownByTicketsTypeReport>(report.Value);
            return result;
        }

        public async Task<List<Month>> GetJsonReportMonthly(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<MonthlyDivisionIntoErrorsOtherAndClosed>(report.Value);

            var listOfMonth = new List<Month>();

            if (result == null) return listOfMonth;
            
            foreach ((var key, var value) in result.Month)
            {
                var newMonth = new Month
                {
                    NameOfMonth = key,
                    CountOfCreatedTickets = (int) value.Where(x => x.Key == "Всего").Select(y => y.Value).FirstOrDefault(),
                    CountOfCreatedTicketsWithTypeError = (int) value.Where(x => x.Key == "Ошибки").Select(y => y.Value).FirstOrDefault(),
                    CountOfTicketsWithOtherTypes = (int) value.Where(x => x.Key == "Другое").Select(y => y.Value).FirstOrDefault(),
                    CountOfClosedTicketsInThisMonth = (int) value.Where(x => x.Key == "Закрыто в этом месяце").Select(y => y.Value).FirstOrDefault(),
                    Difference = (int) value.Where(x => x.Key == "Разница").Select(y => y.Value).FirstOrDefault()
                };
                listOfMonth.Add(newMonth);
            }

            return listOfMonth;
        }

        public async Task<List<Module>> GetJsonReportModularly(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<Modularly>(report.Value);

            var listOfModules = new List<Module>();

            if (result == null) return listOfModules;
            
            foreach ((var key, var value) in result.Modules)
            {
                var newModule = new Module
                {
                    Name = key,
                    Percent = value.Where(x => x.Key == "В%").Select(y => y.Value).FirstOrDefault(),
                    Total = (int) value.Where(x => x.Key == "Всего").Select(y => y.Value).FirstOrDefault(),
                    CountOfClosedStatus = (int) value.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault(),
                    CountOfOpenedStatus = (int) value.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault(),
                    CountOfOtherStatus = (int) value.Where(x => x.Key == "Другое").Select(y => y.Value).FirstOrDefault()
                };
                listOfModules.Add(newModule);
            }

            return listOfModules;
        }

        public async Task<List<ClientAndModules>> GetJsonReportClientAndModule(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<DistributionOfOpenOrdersByClientsAndModules>(report.Value);

            var listOfClientAndModules = new List<ClientAndModules>();

            if (result == null) return listOfClientAndModules;
            
            foreach ((var key, var value) in result.Company)
            {
                var newModule = new ClientAndModules
                {
                    Name = key,
                    Number = (int) value.Where(x => x.Key == "№п/п").Select(y => y.Value).FirstOrDefault(),
                    Credits = (int) value.Where(x => x.Key == "Кредиты").Select(y => y.Value).FirstOrDefault(),
                    RKO = (int) value.Where(x => x.Key == "РКО").Select(y => y.Value).FirstOrDefault(),
                    AUR = (int) value.Where(x => x.Key == "АУР").Select(y => y.Value).FirstOrDefault(),
                    AccountingDepartment = (int) value.Where(x => x.Key == "Бухгалтерия").Select(y => y.Value).FirstOrDefault(),
                    DepositsSettlement = (int) value.Where(x => x.Key == "Депозиты,расчетные").Select(y => y.Value).FirstOrDefault(),
                    IB = (int) value.Where(x => x.Key == "ИБ").Select(y => y.Value).FirstOrDefault(),
                    Carts = (int) value.Where(x => x.Key == "Карты").Select(y => y.Value).FirstOrDefault(),
                    Cashbox = (int) value.Where(x => x.Key == "Касса").Select(y => y.Value).FirstOrDefault(),
                    Clients = (int) value.Where(x => x.Key == "Клиенты").Select(y => y.Value).FirstOrDefault(),
                    Complaens = (int) value.Where(x => x.Key == "Комплаенс").Select(y => y.Value).FirstOrDefault(),
                    ReferenceData = (int) value.Where(x => x.Key == "Справочные данные").Select(y => y.Value).FirstOrDefault(),
                    Service = (int) value.Where(x => x.Key == "Сервис").Select(y => y.Value).FirstOrDefault(),
                    Reports = (int) value.Where(x => x.Key == "Отчеты").Select(y => y.Value).FirstOrDefault(),
                    Pledges = (int) value.Where(x => x.Key == "Залоги").Select(y => y.Value).FirstOrDefault(),
                    PRBO = (int) value.Where(x => x.Key == "ПРБО").Select(y => y.Value).FirstOrDefault(),
                    BasicGuides = (int) value.Where(x => x.Key == "Базовые справочники").Select(y => y.Value).FirstOrDefault(),
                    EWallet = (int) value.Where(x => x.Key == "Эл.кошелек").Select(y => y.Value).FirstOrDefault(),
                    ACP = (int) value.Where(x => x.Key == "ЭЦП").Select(y => y.Value).FirstOrDefault(),
                    IntegrationService = (int) value.Where(x => x.Key == "Integration Service").Select(y => y.Value).FirstOrDefault(),
                    Total = (int) value.Where(x => x.Key == "Итого").Select(y => y.Value).FirstOrDefault()
                };
                listOfClientAndModules.Add(newModule);
            }

            return listOfClientAndModules;
        }

        public async Task<List<ReportInDynamics>> GetJsonReportInDynamic(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<StatusesForTodayInDynamicsCheckModel>(report.Value);

            return (from status in _statuses
                    let dictionary = GetStatusDictionary(status, result)
                    where dictionary != null
                    select new ReportInDynamics
                    {
                               Status = status,
                               Total = (int) dictionary.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault(),
                               Percent = (int) dictionary.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault(),
                               PreviosDayGet = (int) dictionary.Where(x => x.Key == "По сравнению с предыдущим днем(Приход)").Select(y => y.Value).FirstOrDefault(),
                               PreviosDaySet = (int) dictionary.Where(x => x.Key == "По сравнению с предыдущим днем(Расход)").Select(y => y.Value).FirstOrDefault(),
                               ForWeekGet = (int) dictionary.Where(x => x.Key == "За неделю(Приход)").Select(y => y.Value).FirstOrDefault(),
                               ForWeekSet = (int) dictionary.Where(x => x.Key == "За неделю(Расход)").Select(y => y.Value).FirstOrDefault(),
                               ForMonthkGet = (int) dictionary.Where(x => x.Key == "За месяц(Приход)").Select(y => y.Value).FirstOrDefault(),
                               ForForMonthSet = (int) dictionary.Where(x => x.Key == "За месяц(Расход)").Select(y => y.Value).FirstOrDefault()
                           }).ToList();
        }

        public async Task<List<ReportsByAgents>> GetJsonReportByAgents(string name)
        {
            var report = await _reportsContext.Reports.Where(x => x.Name == name).FirstOrDefaultAsync();

            var result = JsonSerializer.Deserialize<ReportByAgentsCheckModel>(report.Value);

            var listOfReportsByAgents = new List<ReportsByAgents>();

            foreach (var agent in _agents)
            {
                var dictionary = GetAgentDictionary(agent, result);

                if (dictionary == null)
                    continue;
                var newAgent = new ReportsByAgents
                {
                    Name = agent,
                    OpenGet = dictionary.Where(x => x.Key == "Открыто(Приход)").Select(y => y.Value).FirstOrDefault(),
                    OpenSet = dictionary.Where(x => x.Key == "Открыто(Расход)").Select(y => y.Value).FirstOrDefault(),
                    OpenStatic = dictionary.Where(x => x.Key == "Открыто(Остаток)").Select(y => y.Value).FirstOrDefault(),

                    AwaitGet = dictionary.Where(x => x.Key == "В ожидании(Приход)").Select(y => y.Value).FirstOrDefault(),
                    AwaitSet = dictionary.Where(x => x.Key == "В ожидании(Расход)").Select(y => y.Value).FirstOrDefault(),
                    AwaitStatic = dictionary.Where(x => x.Key == "В ожидании(Остаток)").Select(y => y.Value).FirstOrDefault(),

                    AwaitingEvaluationGet = dictionary.Where(x => x.Key == "Ожидает оценки(Приход)").Select(y => y.Value).FirstOrDefault(),
                    AwaitingEvaluationSet = dictionary.Where(x => x.Key == "Ожидает оценки(Расход)").Select(y => y.Value).FirstOrDefault(),

                    AssessmentInProgressGet = dictionary.Where(x => x.Key == "Идет оценка(Приход)").Select(y => y.Value).FirstOrDefault(),
                    AssessmentInProgressSet = dictionary.Where(x => x.Key == "Идет оценка(Расход)").Select(y => y.Value).FirstOrDefault(),


                    AssessmentSentGet = dictionary.Where(x => x.Key == "Оценка отправлена(Приход)").Select(y => y.Value).FirstOrDefault(),
                    AssessmentSentSet = dictionary.Where(x => x.Key == "Оценка отправлена(Расход)").Select(y => y.Value).FirstOrDefault(),

                    Reject = dictionary.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault(),

                    OrderConfirmedGet = dictionary.Where(x => x.Key == "Заказ подтвержден(Приход)").Select(y => y.Value).FirstOrDefault(),
                    OrderConfirmedSet = dictionary.Where(x => x.Key == "Заказ подтвержден(Расход)").Select(y => y.Value).FirstOrDefault(),

                    InWorkGet = dictionary.Where(x => x.Key == "В работе(Приход)").Select(y => y.Value).FirstOrDefault(),
                    InWorkSet = dictionary.Where(x => x.Key == "В работе(Расход)").Select(y => y.Value).FirstOrDefault(),

                    SubmittedForVerificationGet = dictionary.Where(x => x.Key == "Передано на проверку(Приход)").Select(y => y.Value).FirstOrDefault(),
                    SubmittedForVerificationSet = dictionary.Where(x => x.Key == "Передано на проверку(Расход)").Select(y => y.Value).FirstOrDefault(),

                    FeedbackFromTheClientGet = dictionary.Where(x => x.Key == "Feedback от клиента(Приход)").Select(y => y.Value).FirstOrDefault(),
                    FeedbackFromTheClientSet = dictionary.Where(x => x.Key == "Feedback от клиента(Расход)").Select(y => y.Value).FirstOrDefault(),

                    InTheQueueForDeliveryGet = dictionary.Where(x => x.Key == "В очереди на поставку(Приход)").Select(y => y.Value).FirstOrDefault(),
                    InTheQueueForDeliverySet = dictionary.Where(x => x.Key == "В очереди на поставку(Расход)").Select(y => y.Value).FirstOrDefault(),

                    Closed = dictionary.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault()
                };
                listOfReportsByAgents.Add(newAgent);
            }
            return listOfReportsByAgents;
        }

        private Dictionary<string, double> GetStatusDictionary(string statusname, StatusesForTodayInDynamicsCheckModel result) => statusname switch
        {
            "Открыто" => result.Open,
            "В ожидании" => result.Awaiting,
            "Ожидает оценки" => result.AwaitingEvaluation,
            "Идет оценка" => result.AssessmentInProgress,
            "Оценка отправлена" => result.AssessmentSent,
            "Отказ" => result.Reject,
            "Заказ подтвержден" => result.OrderConfirmed,
            "В работе" => result.InWork,
            "Передано на проверку" => result.SubmittedForVerification,
            "Feedback от клиента" => result.FeedbackFromTheClient,
            "В очереди на поставку" => result.InTheQueueForDelivery,
            "Закрыто" => result.Closed,
            _ => null
        };

        private Dictionary<string, int> GetAgentDictionary(string agentname, ReportByAgentsCheckModel result) => agentname switch
        {
            "Артем Чупахин" => result.ArtemChupahin,
            "Аида Абыкеева" => result.AidaAbykeeva,
            "Гульзана Эсеналиева" => result.GulzanaEsenalieva,
            "Жоомарт Шарабидинов" => result.JoomartSharabidinov,
            "Мээрим Нурлантбекова" => result.MeerimNurlantbekova,
            "Ильяс Джиенбаев" => result.IlyasJienbaev,
            _ => null
        };
    }
}
