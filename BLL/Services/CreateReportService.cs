﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Settings;
using DAL.Models;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace BLL.Services
{
    public class CreateReportService
    {
        private static readonly DateTime DateNow = DateTime.Now;
        private static readonly DateTime StartDate = new DateTime(DateNow.Year, 01, 01);
        private static readonly int EndMonth = Math.Max(1, DateNow.Month - 1);
        private static readonly DateTime EndDate = new DateTime(DateNow.Year, EndMonth, DateTime.DaysInMonth(DateNow.Year, EndMonth)).AddDays(1);
        private readonly List<string> _companies;
        private readonly TicketService _ticketService;
        private readonly HtmlService _htmlService;
        private readonly List<Agent> _agents;
        private readonly Dictionary<int, string> _statuses;
        private readonly List<string> _modules;
        private readonly CreateJsonReportsService _createJsonReportsService;

        public CreateReportService(SystemCollections systemCollections, TicketService ticketService, HtmlService htmlService, CreateJsonReportsService createJsonReportsService)
        {
            _createJsonReportsService = createJsonReportsService;
            _companies = systemCollections.Companies;
            _statuses = systemCollections.Statuses;
            _modules = systemCollections.Modules;
            _agents = systemCollections.Agents;
            _ticketService = ticketService;
            _htmlService = htmlService;
        }

        public async Task CreateReportBreakdownByTicketsTypeAsync(List<Ticket> listOfTickets)
        {
            var globalReport = new Dictionary<string, int>
            {
                                   {"Ошибка/bug", 0},
                                   {"Запрос на изменение", 0},
                                   {"Вопрос по системе", 0},
                                   {"Запрос нового функционала", 0},
                                   {"Вопрос по параметризации", 0}
                               };

            var countOfRecordsInGlobalReport = 0;

            var keys = globalReport.Keys.ToList();

            for (var i = 0; i < 5; i++)
            {
                var key = keys[i];
                globalReport[key] = listOfTickets.Count(x => x.type == key && x.created_at >= StartDate && x.created_at <= EndDate);

                countOfRecordsInGlobalReport += globalReport[key];
            }

            await _createJsonReportsService.CreateJsonReportBreakdownByTicketsTypeAsync(globalReport, countOfRecordsInGlobalReport);

            await _htmlService.CreateHtmlFileBreakdownByTicketsTypeAsync(globalReport, countOfRecordsInGlobalReport);
        }

        public List<Agent> CreateReportDistributionOfOpenTicketsByAgents(List<Ticket> listOfTickets)
        {
            foreach (var agent in _agents)
            {
                foreach ((var key, var value) in _statuses)
                {
                    if (!agent.tickets.ContainsKey(value))
                        agent.tickets.Add(value, listOfTickets.Where(x => x.responder_id == agent.id && x.status == key).Select(y => y.id).ToList());
                }
            }

            return _agents;
        }

        public List<Status> CreateReportStatusesForTodayInDynamics(List<Ticket> listOfTickets)
        {
            var listOfStatuses = new List<Status>();

            foreach ((var key, var value) in _statuses)
            {
                var status = new Status
                             {
                                 Name = value,
                                 Total = listOfTickets.Where(x => x.status == key)
                                                      .Select(y => (double) y.id)
                                                      .ToList(),
                                 CountFromTheBeginningOfTheMonth = listOfTickets.Where(x => x.status == key
                                         && x.created_at >=
                                         new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01)
                                         && x.created_at <=
                                         new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                                      DateTime.DaysInMonth(DateNow.Year, DateNow.Month)))
                                    .Select(y => (double) y.id)
                                    .ToList()
                             };



                var startWeek = DateTime.Today.AddDays(-(int) DateTime.Today.DayOfWeek + (int) DayOfWeek.Monday);

                status.CountForWeek = listOfTickets.Where(x => x.status == key
                                                               && x.created_at >= startWeek
                                                               && x.created_at <= startWeek.AddDays(7))
                                                                                            .Select(y => (double) y.id)
                                                                                            .ToList();

                status.CountOfPreviousDayDifference = listOfTickets.Where(x => x.status == key)
                    .Select(y => (double) y.id)
                    .ToList();

                listOfStatuses.Add(status);
            }

            return listOfStatuses;
        }

        public async Task MonthlyDivisionIntoErrorsOtherAndClosedAsync(List<Ticket> listOfTickets)
        {
            var start = new DateTime(DateTime.Now.Year, 01, 01);

            var reportsByMonth = new List<Month>();

            do
            {
                var end = new DateTime(DateTime.Now.Year, start.Month, DateTime.DaysInMonth(DateTime.Now.Year, start.Month));

                var month = new Month
                            {
                                NumberOfMonth = start.Month,
                                CountOfCreatedTickets = listOfTickets.Count(x => x.created_at > start
                                                                                && x.created_at <= end
                                                                                   .AddDays(1)),
                                CountOfCreatedTicketsWithTypeError = listOfTickets.Count(x => x.type == "Ошибка/bug" &&
                                    x.created_at > start
                                    && x.created_at <= end
                                       .AddDays(1))
                            };


                month.CountOfTicketsWithOtherTypes =
                    month.CountOfCreatedTickets - month.CountOfCreatedTicketsWithTypeError;

                month.CountOfClosedTicketsInThisMonth =
                    listOfTickets.Count(x => x.status == 4 && x.created_at > start
                                                           && x.created_at <= end
                                                               .AddDays(1)) + listOfTickets.Count(x => x.status == 5 && x.created_at > start
                                                                                                                     && x.created_at <= end
                                                                                                                         .AddDays(1));
                month.CountOfCustomization = listOfTickets.Count(x => x.tags.Contains("Кастомизация") && x.created_at > start
                                                                                                      && x.created_at <= end
                                                                                                          .AddDays(1));

                month.Difference = month.CountOfCreatedTickets - month.CountOfClosedTicketsInThisMonth;

                month.PercentOfCountOfCustomization =
                    Math.Round((double) month.CountOfCustomization / month.CountOfCreatedTickets * 100d,
                        1);

                month.PercentOfTicketsWithTypeError =
                    Math.Round((double) month.CountOfCreatedTicketsWithTypeError / month.CountOfCreatedTickets * 100d,
                        1);
                month.PercentOfTicketsWithOtherType =
                    Math.Round((double) month.CountOfTicketsWithOtherTypes / month.CountOfCreatedTickets * 100d, 1);

                reportsByMonth.Add(month);

                start = new DateTime(DateTime.Now.Year, start.Month + 1, 01);

            } while (start.Month != DateTime.Now.Month);

            await _createJsonReportsService.CreateJsonReportMonthlyDivisionIntoErrorsOtherAndClosedAsync(reportsByMonth);

            await _htmlService.CreateHtmlFileMonthlyDivisionIntoErrorsOtherAndClosedAsync(reportsByMonth);
        }

        public async Task CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync()
        {
            var responseBody = _ticketService.CreateNewRequestGetCompanies();
            var listOfCompany = JsonSerializer.Deserialize<List<Company>>(responseBody);
            
            if (listOfCompany == null) return;

            foreach (var company in listOfCompany)
            {
                if (!_companies.Contains(company.name)) continue;

                var page = 1;
                do
                {
                    responseBody = _ticketService.CreateNewRequestWithCompanyId(page, company.id);
                    var listOfTickets = JsonSerializer.Deserialize<List<Ticket>>(responseBody);

                    if (listOfTickets == null) continue;
                    
                    foreach (var ticket in listOfTickets)
                    {
                        if (ticket.status == 4 || ticket.status == 5 || ticket.type == null) continue;

                        if (ticket.custom_fields.tf == null || !company.Modules.ContainsKey(ticket.custom_fields.tf)) continue;

                        company.Modules[ticket.custom_fields.tf]++;
                    }

                    page++;
                } while (responseBody != "[]");
            }
            await _createJsonReportsService.CreateJsonReportDistributionOfOpenOrdersByClientsAndModulesAsync(listOfCompany, _companies);

            await _htmlService.CreateHtmlFileDistributionOfOpenOrdersByClientsAndModulesAsync(listOfCompany, _companies);
        }

        public async Task CreateReportModularlyAsync(List<Ticket> listOfTickets)
        {
            var listOfModules = new List<Module>();

            foreach (var module in _modules)
            {
                if (module == "Отчеты") continue;

                var moduleRecord = new Module
                                   {
                                       Name = module,
                                       Total = listOfTickets.Count(x => x.custom_fields.tf == module),
                                       CountOfOpenedStatus = listOfTickets.Count(x => x.status == 2 && x.custom_fields.tf == module),
                                       CountOfClosedStatus = listOfTickets.Count(x => x.status == 4 || x.status == 5 && x.custom_fields.tf == module)
                                   };

                moduleRecord.CountOfOtherStatus = (moduleRecord.Total -
                                                   (moduleRecord.CountOfClosedStatus +
                                                    moduleRecord.CountOfOpenedStatus));
                moduleRecord.Percent = Math.Round(((double) moduleRecord.Total / listOfTickets.Count) * 100d, 1);

                listOfModules.Add(moduleRecord);
            }

            await _createJsonReportsService.CreateJsonReportModularlyAsync(listOfModules);

            await _htmlService.CreateHtmlFileModularlyAsync(listOfModules);
        }
    }
}
