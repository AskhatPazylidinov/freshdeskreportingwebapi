﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using DAL.Context;
using DAL.Models.CheckChangesModels;
using DAL.Models.ReportModels;

namespace BLL.Services
{
    public class CheckChangesOfReportsService
    {
        private readonly ReportsContext _reportsContext;
        public CheckChangesOfReportsService(ReportsContext reportsContext)
        {
            _reportsContext = reportsContext;
        }

        public StatusesForTodayInDynamicsCheckModel CheckChangesOfReportStatusesForTodayInDynamics(StatusesForTodayInDynamics checkReport)
        {
            var prevReports = _reportsContext.ReportsPreviosDays.Where(x => x.Name == checkReport.Name).ToList();
            var rep = prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
            
            var previousWr = prevReports.FirstOrDefault(x => x.DateOfCreate <= DateTime.Now.AddDays(-7));
            var previousMr = prevReports.FirstOrDefault(x => x.DateOfCreate <= DateTime.Now.AddMonths(-1));
            
            
            StatusesForTodayInDynamics previousDayReport = null;
            var previousWeekReport = JsonSerializer.Deserialize<StatusesForTodayInDynamics>(previousWr.Value);
            var previousMonthReport = JsonSerializer.Deserialize<StatusesForTodayInDynamics>(previousMr.Value);
            if (rep != null)
            {
                previousDayReport = JsonSerializer.Deserialize<StatusesForTodayInDynamics>(rep.Value);
            }
            else
            {
                rep = prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd")) ??
                      prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd"));
                if (rep != null) previousDayReport = JsonSerializer.Deserialize<StatusesForTodayInDynamics>(rep.Value);
            }

            var reportStatusesForTodayInDynamics = new StatusesForTodayInDynamicsCheckModel
            {
                Name = "Статусы на сегодня в динамике",
                Open = new Dictionary<string, double>
                {
                    {
                        checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.Open.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Open.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport?.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Open.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                Awaiting = new Dictionary<string, double>
                {
                   {
                        checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.Awaiting.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Awaiting.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Awaiting.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                AwaitingEvaluation = new Dictionary<string, double>
                {
                     {
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AwaitingEvaluation.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                AssessmentInProgress = new Dictionary<string, double>
                {
                    {
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentInProgress.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                AssessmentSent = new Dictionary<string, double>
                {
                     {
                        checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.AssessmentSent.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.AssessmentSent.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.AssessmentSent.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                Reject = new Dictionary<string, double>
                {
                    {
                        checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.Reject.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Reject.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Reject.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                OrderConfirmed = new Dictionary<string, double>
                {
                   {
                        checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.OrderConfirmed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.OrderConfirmed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.OrderConfirmed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                InWork = new Dictionary<string, double>
                {
                   {
                        checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.InWork.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.InWork.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InWork.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                SubmittedForVerification = new Dictionary<string, double>
                {
                   {
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.SubmittedForVerification.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                FeedbackFromTheClient = new Dictionary<string, double>
                {
                   {
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.FeedbackFromTheClient.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                Resolved = new Dictionary<string, double>
                {
                   {
                        checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.Resolved.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Resolved.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Resolved.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                InTheQueueForDelivery = new Dictionary<string, double>
                {
                    {
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.InTheQueueForDelivery.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                },
                Closed = new Dictionary<string, double>
                {
                   {
                        checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Count()
                    },
                    {
                        checkReport.Closed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Key).FirstOrDefault(),
                        checkReport.Closed.Where(x => x.Key == "Процент от общего кол-ва").Select(y => y.Value).FirstOrDefault().FirstOrDefault()
                    },
                    {
                        "По сравнению с предыдущим днем(Приход)",
                        checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousDayReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "По сравнению с предыдущим днем(Расход)",
                        previousDayReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Приход)",
                        checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousWeekReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За неделю(Расход)",
                        previousWeekReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Приход)",
                        checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(previousMonthReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    },
                    {
                        "За месяц(Расход)",
                        previousMonthReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault().Except(checkReport.Closed.Where(x => x.Key == "Количество").Select(y => y.Value).FirstOrDefault()).Count()
                    }
                }
            };

            return reportStatusesForTodayInDynamics;
        }

        public ReportByAgentsCheckModel CheckChangesOfReportByAgents(DistributionOfOpenTicketsByAgents checkReport)
        {
            var prevReports = _reportsContext.ReportsPreviosDays.Where(x => x.Name == checkReport.Name).ToList();
            var rep = prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
            DistributionOfOpenTicketsByAgents previosReport = null;
            if (rep != null)
            {
                previosReport = JsonSerializer.Deserialize<DistributionOfOpenTicketsByAgents>(rep.Value);
            }
            else
            {
                rep = prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd")) ??
                      prevReports.FirstOrDefault(x => x.DateOfCreate.ToString("yyyy-MM-dd") == DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd"));
                if (rep != null)
                    previosReport = JsonSerializer.Deserialize<DistributionOfOpenTicketsByAgents>(rep.Value);
            }

            var reportByAgents = new ReportByAgentsCheckModel
            {
                Name = checkReport.Name,
                ArtemChupahin = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.ArtemChupahin.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.ArtemChupahin.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.ArtemChupahin.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.ArtemChupahin.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.ArtemChupahin.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.ArtemChupahin.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.ArtemChupahin.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.ArtemChupahin.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                },
                AidaAbykeeva = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.AidaAbykeeva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.AidaAbykeeva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.AidaAbykeeva.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.AidaAbykeeva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.AidaAbykeeva.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                },
                GulzanaEsenalieva = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.GulzanaEsenalieva.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.GulzanaEsenalieva.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.GulzanaEsenalieva.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                },
                JoomartSharabidinov = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.JoomartSharabidinov.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.JoomartSharabidinov.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.JoomartSharabidinov.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.JoomartSharabidinov.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.JoomartSharabidinov.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                },
                MeerimNurlantbekova = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.MeerimNurlantbekova.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.MeerimNurlantbekova.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.MeerimNurlantbekova.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                },
                IlyasJienbaev = new Dictionary<string, int>
                {
                {"Открыто(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Открыто(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},
                {"Открыто(Остаток)", previosReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.IlyasJienbaev.Where(x => x.Key == "Открыто").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В ожидании(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В ожидании(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},
                {"В ожидании(Остаток)", previosReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault().Intersect(checkReport.IlyasJienbaev.Where(x => x.Key == "В ожидании").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Ожидает оценки(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Ожидает оценки(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Ожидает оценки").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Идет оценка(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Идет оценка(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Идет оценка").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Оценка отправлена(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Оценка отправлена(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Оценка отправлена").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Отказ", checkReport.IlyasJienbaev.Where(x => x.Key == "Отказ").Select(y => y.Value).FirstOrDefault().Count()},

                {"Заказ подтвержден(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Заказ подтвержден(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Заказ подтвержден").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В работе(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В работе(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "В работе").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Передано на проверку(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Передано на проверку(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Передано на проверку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Feedback от клиента(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count() },
                {"Feedback от клиента(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "Feedback от клиента").Select(y => y.Value).FirstOrDefault()).Count()},

                {"В очереди на поставку(Приход)", checkReport.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(previosReport.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count() },
                {"В очереди на поставку(Расход)", previosReport.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault().Except(checkReport.IlyasJienbaev.Where(x => x.Key == "В очереди на поставку").Select(y => y.Value).FirstOrDefault()).Count()},

                {"Закрыто", checkReport.IlyasJienbaev.Where(x => x.Key == "Закрыто").Select(y => y.Value).FirstOrDefault().Count()}
                }
            };

            return reportByAgents;
        }
    }
}
