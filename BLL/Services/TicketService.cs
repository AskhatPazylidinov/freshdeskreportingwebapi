﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using BLL.Settings;
using DAL.Models;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace BLL.Services
{
    public class TicketService
    {
        private readonly List<string> _companies;

        public TicketService(SystemCollections settings)
        {
            _companies = settings.Companies;
        }

        public List<Ticket> GetListOfTickets()
        {
            var listOfTickets = new List<Ticket>();

            var responseBody = CreateNewRequestGetCompanies();
            var listOfCompany = JsonSerializer.Deserialize<List<Company>>(responseBody);

            if (listOfCompany == null) return listOfTickets;
            
            foreach (var company in listOfCompany)
            {
                if (!_companies.Contains(company.name)) continue;

                var page = 1;
                do
                {
                    responseBody = CreateNewRequestWithCompanyId(page, company.id);
                    listOfTickets.AddRange(JsonSerializer.Deserialize<List<Ticket>>(responseBody));
                    page++;
                } while (responseBody != "[]");
            }

            return listOfTickets;
        }

        public string CreateNewRequestGetCompanies()
        {
            var apiPath = $"/api/v2/companies";
            return ExecuteRequest(apiPath);
        }

        public string CreateNewRequestWithCompanyId(int newPage, double companyId)
        {
            var apiPath = $"/api/v2/tickets?company_id={companyId}&per_page=100&page={newPage}";
            return ExecuteRequest(apiPath);
        }

        private string ExecuteRequest(string apiPath)
        {
            var request = (HttpWebRequest) WebRequest.Create("https://financesoft.freshdesk.com" + apiPath);
            request.ContentType = "application/json";
            request.Method = "GET";
            request.Headers["Authorization"] = "Basic b0tOTUt1QkRsUEZpQnZVVmdEMjc6WA==";

            string responseBody;

            using (var response = (HttpWebResponse) request.GetResponse())
            {
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);

                responseBody = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            return responseBody;
        }
    }
}
