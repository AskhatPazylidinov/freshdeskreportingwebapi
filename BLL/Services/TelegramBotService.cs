﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BLL.Enums;
using DAL.Context;
using DAL.Models;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using File = System.IO.File;

namespace BLL.Services
{
    public class TelegramBotService
    {
        private static string Token { get; } = "1898970636:AAHade0hLsSI2gSbFnq2KqX82Uvde5u9ciI";
        protected static TelegramBotClient Client;

        private StateOfBot _stateOfBot;
        private SelectedAct _selectedAct;

        private readonly string _listOfFunctions = string.Empty;
        protected readonly List<string> Reports = new List<string>
        {
                                                      "Разбивка_по_типам_заявок",
                                                      $"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now:yyyy - MM - dd})",
                                                      $"Статусы_на_сегодня_в_динамике_({DateTime.Now:yyyy-MM-dd})",
                                                      $"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now:yyyy - MM - dd})",
                                                      "Распределение_незакрытых_заявок_по_клиентам_и_модулям",
                                                      "Помодульно"
        };
        private List<string> Functions { get; set; } = new List<string>
        {
            "1-Создать Отчет",
            "2-Подписать на рассылку Отчетов",
            "3-Отключить рассылку отчетов"
        };

        private readonly CreateReportService _createReportService;
        protected readonly ReportsContext ReportsContext;
        private readonly TicketService _ticketService;

        public TelegramBotService(CreateReportService createReportService, ReportsContext reportsContext, TicketService ticketService)
        {
            _createReportService = createReportService;
            ReportsContext = reportsContext;
            _ticketService = ticketService;

            foreach (var function in Functions)
                _listOfFunctions += $"{function}\n";
        }

        public void StartBot()
        {
            _stateOfBot = StateOfBot.SelectOfFunction;
            Client = new TelegramBotClient(Token);
            Client.StartReceiving();
            Client.OnMessage += OnMessageHandler;
            while (true)
                Thread.Sleep(10000);
        }

        private async void OnMessageHandler(object sender, MessageEventArgs e)
        {
            var msg = e.Message;
            if (msg.Text == null)
                return;

            switch ((int) _stateOfBot)
            {
                case 1:
                    await Client.SendTextMessageAsync(msg.Chat.Id, $"Выберите номер действия!\n{_listOfFunctions}", replyMarkup: ClearButtons());
                    _stateOfBot = StateOfBot.SelectOfAct;
                    break;
                case 2:
                    await SelectFunction(msg);
                    _stateOfBot = StateOfBot.Executing;
                    break;
                case 3:
                    await ExecutingAct(msg);
                    break;
            }
        }

        private async Task SelectFunction(Message msg)
        {
            var result = msg.Text.Replace("/", "");
            switch (result)
            {
                case "1":
                    await Client.SendTextMessageAsync(msg.Chat.Id, "Выберите тип отчета!", replyMarkup: GetButtons());
                    _selectedAct = SelectedAct.CreateReports;
                    break;
                case "2":
                    await Client.SendTextMessageAsync(msg.Chat.Id, "Укажите свою фамилию!", replyMarkup: ClearButtons());
                    _selectedAct = SelectedAct.EnableDistributionReports;
                    break;
                case "3":
                    await DeleteFromListOfEmployees(msg);
                    break;
                default:
                    await Client.SendTextMessageAsync(msg.Chat.Id, $"Выберите верное действие!\n{_listOfFunctions}", replyMarkup: ClearButtons());
                    _stateOfBot = StateOfBot.SelectOfAct;
                    break;
            }
        }

        private async Task ExecutingAct(Message msg)
        {
            switch ((int) _selectedAct)
            {
                case 1:
                    await CreateReport(msg);
                    break;
                case 2:
                    await AddToListOfEmployees(msg);
                    break;
            }

        }

        private async Task DeleteFromListOfEmployees(Message msg)
        {
            var employees = ReportsContext.TableOfEmployees.FirstOrDefault(x => x.ChatId == msg.Chat.Id);

            if (employees != null)
            {
                ReportsContext.TableOfEmployees.Remove(employees);
                await ReportsContext.SaveChangesAsync();
                await Client.SendTextMessageAsync(msg.Chat.Id, "Вы отключены от рассылки!", replyMarkup: ClearButtons());
            }
            else
            {
                await Client.SendTextMessageAsync(msg.Chat.Id, "Рассылка уже отключена!", replyMarkup: ClearButtons());
            }
            _stateOfBot = StateOfBot.SelectOfFunction;
        }

        private async Task AddToListOfEmployees(Message msg)
        {
            var employees = ReportsContext.TableOfEmployees.FirstOrDefault(x => x.ChatId == msg.Chat.Id);

            if (employees == null)
            {
                var employee = new TableOfEmployees
                {
                    Name = msg.Text,
                    ChatId = msg.Chat.Id
                };
                ReportsContext.TableOfEmployees.Add(employee);
                await ReportsContext.SaveChangesAsync();

                await Client.SendTextMessageAsync(msg.Chat.Id, "Вы подписаны на рассылку!", replyMarkup: ClearButtons());
                _stateOfBot = StateOfBot.SelectOfFunction;
            }
            else
            {
                await Client.SendTextMessageAsync(msg.Chat.Id, "Вы уже подписаны на рассылку отчетов!", replyMarkup: ClearButtons());
                _stateOfBot = StateOfBot.SelectOfFunction;
            }
        }

        private async Task CreateReport(Message msg)
        {
            await Client.SendTextMessageAsync(msg.Chat.Id, "Ожидайте, идет формирование отчета/отчетов!", replyMarkup: ClearButtons());
            var result = await SelectReportType(msg.Text);

            if (result == null) 
                return;
            

            if (result == "Все типы")
                foreach (var report in Reports)
                    await SendCreatedReport(report, msg);
            else if (result == "Error")
                await Client.SendTextMessageAsync(msg.Chat.Id, "Тип отчета неверный!", replyMarkup: GetButtons());
            else
                await SendCreatedReport(result, msg);
            _stateOfBot = StateOfBot.SelectOfFunction;
        }

        private async Task SendCreatedReport(string report, Message msg)
        {
            var path = $"C:\\FreshdeskReportsAPI\\Reports\\{report}.html";
            var fileInf = new FileInfo(path);
            if (fileInf.Exists)
                await using (var sendFileStream = File.Open(path, FileMode.Open))
                    await Client.SendDocumentAsync(msg.Chat.Id, new InputOnlineFile(sendFileStream, path));
            await Client.SendTextMessageAsync(msg.Chat.Id, "Отчет сформирован!", replyMarkup: ClearButtons());
        }

        protected IReplyMarkup ClearButtons() => new ReplyKeyboardRemove();

        private IReplyMarkup GetButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Распределение незакрытых заявок по агентам")
                    },
                    new List<KeyboardButton>
                    {
                    new KeyboardButton("Статусы на 9:00 утра сегодня (дд_мм_гггг) в динамике")
                    },
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Распределение незакрытых заявок по клиентам и модулям")
                    },
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода")
                    },
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Помодульно")
                    },
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Разбивка по типам заявок")
                    },
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Все типы")
                    }
                }
            };
        }

        private async Task<string> SelectReportType(string type)
        {
            var listOfTickets = _ticketService.GetListOfTickets();
            switch (type)
            {
                case "Распределение незакрытых заявок по агентам":
                    _createReportService.CreateReportDistributionOfOpenTicketsByAgents(listOfTickets);
                    return $"Распределние_незакрытых_заявок_по_агентам_({DateTime.Now:yyyy - MM - dd})";

                case "Статусы на 9:00 утра сегодня (дд_мм_гггг) в динамике":
                    _createReportService.CreateReportStatusesForTodayInDynamics(listOfTickets);
                    return $"Статусы_на_сегодня_в_динамике_({DateTime.Now:yyyy-MM-dd})";

                case "Распределение незакрытых заявок по клиентам и модулям":
                    await _createReportService.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync();
                    return $"Распределение_незакрытых_заявок_по_клиентам_и_модулям";

                case "Статистика за период с 01 января текущего года по последний день месяца последнего отчетного периода":
                    await _createReportService.MonthlyDivisionIntoErrorsOtherAndClosedAsync(listOfTickets);
                    return $"Статистика_за_период_с_января_текущего_года_по_последний_день_месяца_последнего_отчетного_периода_({DateTime.Now:yyyy - MM - dd})";

                case "Помодульно":
                    await _createReportService.CreateReportModularlyAsync(listOfTickets);
                    return "Помодульно";

                case "Разбивка по типам заявок":
                    await _createReportService.CreateReportBreakdownByTicketsTypeAsync(listOfTickets);
                    return "Разбивка_по_типам_заявок";

                case "Все типы":
                    await CreateAllReports();
                    return "Все типы";

                default:
                    return "Error";
            }
        }

        protected async Task CreateAllReports()
        {
            var listOfTickets = _ticketService.GetListOfTickets();

            Task t1 = Task.Run(() => _createReportService.CreateReportDistributionOfOpenTicketsByAgents(listOfTickets));
            var t2 = Task.Run(() => _createReportService.CreateReportBreakdownByTicketsTypeAsync(listOfTickets));
            Task t3 = Task.Run(() => _createReportService.CreateReportStatusesForTodayInDynamics(listOfTickets));
            var t4 = Task.Run(() => _createReportService.CreateReportModularlyAsync(listOfTickets));
            var t5 = Task.Run(() => _createReportService.MonthlyDivisionIntoErrorsOtherAndClosedAsync(listOfTickets));
            var t6 = Task.Run(() => _createReportService.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync());

            await Task.WhenAll(new[] {t1, t2, t3, t4, t5, t6});
        }
    }
}
