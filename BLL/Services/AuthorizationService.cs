﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Text;
using BLL.DTO;

namespace BLL.Services
{
    public class AuthorizationService
    {
        public ClaimsIdentity GetIdentity(string username, string password)
        {
            var result = VerifyAccount(username, password);

            if (result != 200) return null;

            var person = new UserDto
            {
                             Login = username,
                             Password = password
                         };
            var claims = new List<Claim>
                         {
                             new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login)
                         };
            var claimsIdentity = new ClaimsIdentity(claims, "Token",
                                                    ClaimsIdentity.DefaultNameClaimType,
                                                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }

        private static int VerifyAccount(string username, string password)
        {
            var authInfo = $"{username}:{password}";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            var request = (HttpWebRequest) WebRequest.Create("https://financesoft.freshdesk.com/api/v2/tickets/1");
            request.ContentType = "application/json";
            request.Method = "GET";
            request.Headers["Authorization"] = "Basic " + authInfo;

            try
            {
                int result;

                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    var dataStream = response.GetResponseStream();
                    var reader = new StreamReader(dataStream);

                    reader.Close();
                    dataStream.Close();

                    result = (int) response.StatusCode;
                }

                return result;
            }
            catch (Exception)
            {
                return 401;
            }
        }
    }
}
