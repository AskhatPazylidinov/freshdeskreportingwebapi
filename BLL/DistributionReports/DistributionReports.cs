﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using DAL.Context;
using Quartz;
using Telegram.Bot.Types.InputFiles;

namespace BLL.DistributionReports
{
    public class DistributionReports : TelegramBotService, IJob
    {
        public DistributionReports(CreateReportService createReportService, ReportsContext reportsContext, TicketService ticketService) : base(createReportService, reportsContext, ticketService) { }

        public async Task Execute(IJobExecutionContext context)
        {
            var listOfSubscribers = ReportsContext.TableOfEmployees.ToList();

            foreach (var employee in listOfSubscribers)
            {
                foreach (var report in Reports)
                {
                    var path = $"C:\\Projects\\FreshDeskReportApi\\Reports\\{report}.html";
                    var fileInf = new FileInfo(path);

                    if (fileInf.Exists)
                        await using (var sendFileStream = File.Open(path, FileMode.Open))
                            await Client.SendDocumentAsync(employee.ChatId, new InputOnlineFile(sendFileStream, path));

                    await Client.SendTextMessageAsync(employee.ChatId, "Автоматическая рассылка отчетов!", replyMarkup: ClearButtons());
                }
            }
        }
    }
}
