﻿using System.Threading.Tasks;
using BLL.Services;
using Quartz;

namespace BLL.UpdateReports
{
    public class UpdateAllReportsToday : IJob
    {
        private readonly CreateReportService _createReportService;
        private readonly TicketService _ticketService;
        private readonly CreateJsonReportsService _createJsonReportsService;
        private readonly HtmlService _htmlService;

        public UpdateAllReportsToday(HtmlService htmlService, CreateReportService createReportService, TicketService ticketService, CreateJsonReportsService createJsonReportsService)
        {
            _createReportService = createReportService;
            _ticketService = ticketService;
            _createJsonReportsService = createJsonReportsService;
            _htmlService = htmlService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var listOfTickets = _ticketService.GetListOfTickets();

            await _createReportService.CreateReportDistributionOfOpenOrdersByClientsAndModulesAsync();
            await _createReportService.CreateReportBreakdownByTicketsTypeAsync(listOfTickets);
            await _createReportService.CreateReportModularlyAsync(listOfTickets);
            await _createReportService.MonthlyDivisionIntoErrorsOtherAndClosedAsync(listOfTickets);

            var reportByAgent = _createReportService.CreateReportDistributionOfOpenTicketsByAgents(listOfTickets);
            var reportByAgentsJson = await _createJsonReportsService.CreateJsonReportDistributionOfOpenTicketsByAgentsAsync(reportByAgent);
            await _htmlService.CreateHtmlFileDistributionOfOpenTicketsByAgentsAsync(reportByAgentsJson);
            
            var report = _createReportService.CreateReportStatusesForTodayInDynamics(listOfTickets);
            var reportJson = await _createJsonReportsService.CreateJsonReportStatusesForTodayInDynamicsAsync(report);
            await _htmlService.CreateHtmlFileStatusesForTodayInDynamicsAsync(reportJson);
        }
    }
}
