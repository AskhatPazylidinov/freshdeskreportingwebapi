﻿namespace BLL.Enums
{
    enum StateOfBot
    {
        SelectOfFunction = 1,
        SelectOfAct = 2,
        Executing = 3
    }
}
