﻿namespace BLL.DTO
{
    class UserDto
    {
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}
