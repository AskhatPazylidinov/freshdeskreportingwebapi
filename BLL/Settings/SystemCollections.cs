﻿using System.Collections.Generic;
using DAL.Models;

namespace BLL.Settings
{
    public class SystemCollections
    {
        public List<string> Companies { get; }
        public Dictionary<int, string> Statuses { get; }
        public List<string> Modules { get; }
        public List<Agent> Agents { get; }

        public SystemCollections(List<string> companies, Dictionary<int, string> statuses, List<string> modules, List<Agent> agents)
        {
            Companies = companies;
            Statuses = statuses;
            Modules = modules;
            Agents = agents;
        }
    }
}
